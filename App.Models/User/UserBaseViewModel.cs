﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;

namespace App.Models.User
{
    public class UserBaseViewModel
    {

        public static Expression<Func<Entities.User, UserBaseViewModel>> Selector
        {
            get
            {
                return user => new UserBaseViewModel()
                {
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    Roles = user.UserRoles.Select(x=>x.Role.Name),
                    PhoneNumber = user.PhoneNumber,
                    ProfilePicturePath = user.UserDetail !=null ? user.UserDetail.PicturePath : ""
                };
            }
        }

        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string ProfilePicturePath { get; set; }
        public string PhoneNumber { get; set; }
        public IEnumerable<string> Roles { get; set; }
    }
}
