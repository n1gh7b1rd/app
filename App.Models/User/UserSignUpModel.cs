﻿using App.Common.Constants;
using System.ComponentModel.DataAnnotations;

namespace App.Models.User
{
    public class UserSignUpModel
    {
       
        [Required]
        [RegularExpression(ValidationRegex.Username)]
        public string UserName { get; set; }

        [Required]
        [RegularExpression(ValidationRegex.Password)]
        public string Password { get; set; }

        [Required]
        [RegularExpression(ValidationRegex.NormalSpaced)]
        public string FirstName { get; set; }

        [Required]
        [RegularExpression(ValidationRegex.NormalSpaced)]
        public string LastName { get; set; }

        [Required]
        //[RegularExpression(ValidationRegex.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}
