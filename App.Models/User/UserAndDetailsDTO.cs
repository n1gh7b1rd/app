﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Models.User
{
    public class UserAndDetailsDTO
    {
        public UserInfoDTO userInfo { get; set; }
        public UserDetailsDTO userDetail { get; set; }
    }
}
