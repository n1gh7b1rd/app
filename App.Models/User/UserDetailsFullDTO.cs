﻿using System;

namespace App.Models.User
{
    public class UserDetailsFullDTO
    {
        public int? GenderId { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string PicturePath { get; set; }
    }
}
