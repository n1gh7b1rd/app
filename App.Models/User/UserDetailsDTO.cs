﻿using System;

namespace App.Models.User
{
    public class UserDetailsDTO
    {
        public int? GenderId { get; set; }
        public DateTime? DateOfBirth { get; set; }
    }
}
