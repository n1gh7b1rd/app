﻿using System;
using System.Linq;
using System.Linq.Expressions;

namespace App.Models.User
{
    public class UserSettingsDTO
    {
        public static Expression<Func<Entities.User, UserSettingsDTO>> UserToUserSettingsSelector
        {
            get
            {
                return user => new UserSettingsDTO()
                {
                    Id = user.Id,
                    UserName = user.UserName,
                    FirstName = user.FirstName,
                    LastName = user.LastName,
                    PhoneNumber = user.PhoneNumber,
                    DateOfBirth = user.UserDetail != null ? user.UserDetail.DateOfBirth : null,
                    Picture = user.UserDetail != null ? user.UserDetail.PicturePath : null,
                };
            }
        }

        public int Id { get; set; }
        public string UserName { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string PhoneNumber { get; set; }
        public DateTime? DateOfBirth { get; set; }
        public string Picture { get; set; }
    }
}
