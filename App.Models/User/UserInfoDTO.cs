﻿using App.Common.Constants;
using App.Localization;
using System;
using System.ComponentModel.DataAnnotations;

namespace App.Models.User
{
    public class UserInfoDTO
    {
        [RegularExpression(ValidationRegex.NormalSpaced)]
        public string FirstName { get; set; }

        [RegularExpression(ValidationRegex.NormalSpaced)]
        public string LastName { get; set; }

        //[RegularExpression(ValidationRegex.PhoneNumber)]
        public string PhoneNumber { get; set; }
    }
}
