﻿using System.Collections.Generic;

namespace App.Models.ApiResult
{
    public class ErrorResult
    {
        public IEnumerable<string> Errors { get; set; }
    }
}
