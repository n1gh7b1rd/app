﻿using System;
using App.Core.Repositories;
using App.Entities;
using App.Data;
using System.Linq;
using System.Linq.Expressions;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Data.Entity;

namespace App.Repositories
{
    public abstract class BaseRepository<TEntity> : IBaseRepository<TEntity> where TEntity : class, IEntity<int>
    {
        protected readonly AppDbContext _context;

        public BaseRepository(AppDbContext context)
        {
            _context = context;
        }


        public async Task<bool> SaveChangesAsync()
        {
            return await _context.SaveChangesAsync() > 0;
        }

        private IQueryable<TEntity> GetById(int id)
        {
            return _context.Set<TEntity>()
                .Where(x => x.Id == id);
        }

        public async Task<TResult> GetByIdAsync<TResult>(int id, Expression<Func<TEntity, TResult>> selector)
        {
            return await this.GetById(id)
                .Select(selector)
                .SingleOrDefaultAsync();
        }

        public async Task<TResult> GetByIdAsync<TResult, TNavProperty>(
            int id,
            Expression<Func<TEntity, TResult>> selector,
            Expression<Func<TEntity, TNavProperty>> selectorForInclude)
        {

            return await this.GetById(id)
                .Include(selectorForInclude)
                .Select(selector)
                .SingleOrDefaultAsync();
        }

        public async Task<List<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync();
        }

        public async Task<List<TResult>> GetAllAsync<TResult>(
            Expression<Func<TEntity, TResult>> selector)
        {
            return await _context.Set<TEntity>()
                .Select(selector)
                .ToListAsync();
        }

        public async Task<List<TResult>> GetAllAsync<TResult, TNavProperty>(
            Expression<Func<TEntity, TResult>> selector,
            Expression<Func<TEntity, TNavProperty>> selectorForInclude)
        {
            return await _context.Set<TEntity>()
                .Include(selectorForInclude)
                .Select(selector)
                .ToListAsync();
        }

        public void CreateWithoutSave(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);
        }
        public void CreateWithoutSave(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().AddRange(entities);
        }
        
        public async Task<bool> CreateAsync(TEntity entity)
        {
            CreateWithoutSave(entity);
            return await _context.SaveChangesAsync() > 0;
        }
        public async Task<bool> CreateAllAsync(IEnumerable<TEntity> entities)
        {
            CreateWithoutSave(entities);
            return await _context.SaveChangesAsync() > 0;
        }

        public virtual void Attach(TEntity entity)
        {
            var objectSet = _context.Set<TEntity>();
            var localEntity = objectSet.Local.FirstOrDefault();
            objectSet.Attach(localEntity);
        }

        public void DeleteWithoutSave(TEntity entity)
        {
            _context.Set<TEntity>().Remove(entity);
        }

        public async Task<bool> DeleteAsync(TEntity entity)
        {
            this.DeleteWithoutSave(entity);
            return await _context.SaveChangesAsync() > 0;
        }

    }
}
