﻿using App.Core.Repositories;
using App.Data;
using App.Entities;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System.Linq.Expressions;
using System;
using System.Collections.Generic;

namespace App.Repositories
{
    public class UserRoleRepository : BaseRepository<UserRole>, IUserRoleRepository
    {
        public UserRoleRepository(AppDbContext context)
            : base(context)
        {

        }

        public async Task<List<TResult>> GetUserRolesAsync<TResult>(int userId, Expression<Func<UserRole, TResult>> selector)
        {
            return await _context.UserRoles
               .Where(x => x.UserId == userId)
               .Select(selector)
               .ToListAsync();
        }
    }
}
