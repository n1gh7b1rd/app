﻿using App.Core.Repositories;
using App.Data;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repositories
{
    public class RefreshTokenRepository : BaseRepository<RefreshToken>, IRefreshTokenRepository
    {
        public RefreshTokenRepository(AppDbContext context) : base(context)
        {

        }

        public Task<RefreshToken> FindRefreshTokenAsync(string token)
        {
            return Task.FromResult(_context.RefreshTokens.Where(x => x.Token == token).SingleOrDefault());
        }

        public async Task<bool> CreateRefreshTokenAsync(RefreshToken token)
        {
            var existingTokens = _context.RefreshTokens.Where(r => r.UserName == token.UserName && r.ClientAppId == token.ClientAppId).ToList();

            if (existingTokens != null)
            {
                var result = await RemoveRefreshToken(existingTokens);
            }

            this.CreateWithoutSave(token);

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> RemoveRefreshToken(List<RefreshToken> oldTokens)
        {
            if (oldTokens != null && oldTokens.Any())
            {
                foreach (var token in oldTokens)
                {
                    this.DeleteWithoutSave(token);
                }

                return await _context.SaveChangesAsync() > 0;
            }

            return false;
        }
    }
}
