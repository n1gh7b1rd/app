﻿using App.Core.Repositories;
using App.Data;
using App.Entities;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;
using System;
using System.Linq.Expressions;

namespace App.Repositories
{
    public class UserRepository : BaseRepository<User>, IUserRepository
    {
        public UserRepository(AppDbContext context)
            : base(context)
        {
        }

        public async Task<User> GetByNameAsync(string userName, bool includeRoles = false)
        {
            //TODO Fix This 
            //return await _context.Set<User>().FindAsync(new object[] { userName });
            var users = _context.Users.AsQueryable();

            if (includeRoles)
            {
                users = users
                    .Include(x => x.UserRoles);
            }

            var result = await users.Where(x => x.UserName == userName).SingleOrDefaultAsync();
            return result;
        }

        public User Attach(int id)
        {
            var user = _context.Set<User>().Local.SingleOrDefault(e => e.Id == id);
            if (user == null)
            {
                user = new User
                {
                    Id = id
                };

                this.Attach(user);
            }

            return user;
        }

        public async Task<bool> UpdateAsync(User user)
        {
            var attachedUser = this.Attach(user.Id);
            attachedUser.PasswordHash = user.PasswordHash;
            var entry = _context.Entry<User>(attachedUser);
            entry.Property(u => u.PasswordHash).IsModified = true;

            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<bool> CreateUserDetailsAsync(UserDetail userDetail)
        {
            _context.Set<UserDetail>().Add(userDetail);
            return await _context.SaveChangesAsync() > 0;
        }

        public async Task<TResult> GetByTokenAsync<TResult>(string token, Expression<Func<User, TResult>> selector)
        {
            return await _context.Users
                .Where(u => u.ConfirmationToken == token)
                .Select(selector)
                .SingleOrDefaultAsync();
        }
    }
}
