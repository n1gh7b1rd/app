﻿using App.Core.Repositories;
using App.Data;
using App.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Repositories
{
    public class ClientAppRepository : BaseRepository<ClientApp>, IClientAppRepository
    {
        public ClientAppRepository(AppDbContext context) 
            : base(context)
        {
        }

        //not async on purpose
        public async Task<ClientApp> FindClient(string name)
        {
            return await _context.ClientApps.Where(x => x.Name == name).SingleOrDefaultAsync();
        }
    }
}
