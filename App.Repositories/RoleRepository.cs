﻿using App.Core.Repositories;
using App.Data;
using App.Entities;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace App.Repositories
{
    public class RoleRepository : BaseRepository<Role>, IRoleRepository
    {
        public RoleRepository(AppDbContext context)
            : base(context)
        {
        }
    }
}
