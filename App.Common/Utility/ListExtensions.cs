﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Utility
{
    public static class ListExtensions
    {
        public static List<T> InList<T>(this T item)
        {
            return new List<T> { item };
        }
    }
}
