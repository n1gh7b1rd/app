﻿namespace App.Common.Enums
{
    public enum ClientApplicationTypesEnum
    {
        Web = 1,
        Mobile = 2,
        NotSpecified = 3
    };
}
