﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;

namespace App.Common.Enums.Extensions
{
    public static class EnumExtensions
    {
        public static IEnumerable<T> GetFilteredEnumsValues<T>(IEnumerable<int> values) where T : struct, IConvertible
        {
            if (typeof(T).IsEnum)
            {
                var filteredValues = Enum.GetValues(typeof(T))
                    .Cast<Enum>()
                    .Where(r => values.Contains((Convert.ToInt32(r))))
                    .ToList();
                return filteredValues.Cast<T>();
            }
            else
            {
                throw new ArgumentException("T must be of type Enum");
            }
        }

        public static T ToEnum<T>(this string enumAsString)
        {
            return (T)Enum.Parse(typeof(T), enumAsString);
        }

        public static T ToEnum<T>(this int value)
        {
            var relationStatuses = Enum.GetValues(typeof(T));
            foreach (var item in relationStatuses)
            {
                if (value == (int)item)
                {
                    return (T)item;
                }
            }

            return default(T);
        }

        public static string GetEnumDescription(this Enum value)
        {
            FieldInfo field = value.GetType().GetField(value.ToString());
            object[] attribs = field.GetCustomAttributes(typeof(DescriptionAttribute), true);
            if (attribs.Length > 0)
            {
                return ((DescriptionAttribute)attribs[0]).Description;
            }
            return string.Empty;
        }

        public static List<int> GetAllEnumValues<T>() where T : struct, IConvertible
        {
            if (typeof(T).IsEnum)
            {
                List<int> AllEnumValues = Enum.GetValues(typeof(T))
                                            .Cast<Enum>()
                                            .Select(e =>
                                            {
                                                return Convert.ToInt32(e);
                                            })
                                            .ToList();
                return AllEnumValues;
            }
            else
            {
                throw new ArgumentException("Enum type expected.");
            }
        }
    }
}
