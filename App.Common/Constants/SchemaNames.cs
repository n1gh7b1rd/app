﻿

namespace App.Common.Constants.Database
{
    public class SchemaNames
    {
        public const string alert = "alert";
        public const string audit = "audit";
        public const string group = "group";
        public const string device = "device";
        public const string glucose = "glucose";
        public const string message = "message";
        public const string oauth = "oauth";
        public const string point = "point";
        public const string relationship = "relationship";
        public const string reward = "reward";
        public const string survey = "survey";
        public const string user = "user";
        public const string dbo = "dbo";
    }
}
