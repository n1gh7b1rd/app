﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Common.Constants
{
    public class ValidationRegex
    {
        public const string PhoneNumber = "^\\+?[1-9]\\d{1,14}$";

        public const string Username = "^\\w+([-+.']\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*$";

        public const string Password = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)[!@#$%\\-_a-zA-Z\\d]{6,}$";

        /// <summary>
        /// This regex requires no starting or trailing spaces or double spaces.
        /// Acceptable: "I like pizza".
        /// Not acceptable: " I like pizza", "I  like pizza", "I like pizza "
        /// </summary>
        public const string NormalSpaced = "^[^\\s]+(\\s{1}[^\\s]+)*$";
    }
}
