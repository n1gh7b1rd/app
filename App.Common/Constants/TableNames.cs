﻿

namespace App.Common.Constants.Database
{
    public class TableNames
    {
        public const string Messages = "Messages";
        public const string MessageRecipients = "MessageRecipients";
        public const string MessageStatusTypes = "MessageStatusTypes";
        public const string MessageTypes = "MessageTypes";
        public const string ClientApps = "ClientApps";
        public const string ClientApplicationTypes = "ClientApplicationTypes";
        public const string RefreshTokens = "RefreshTokens";
        public const string Roles = "Roles";
        public const string Users = "Users";
        public const string UserDetails = "UserDetails";
        public const string UserRoles = "UserRoles";
    }
}
