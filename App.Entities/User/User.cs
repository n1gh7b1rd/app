﻿using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.AspNet.Identity;
using System;
using System.ComponentModel;
using App.Common.Constants.Database;

namespace App.Entities
{
    [Table(DbTableName)]
    public class User : IEntity<int>, IUser<int>
    {
        public const string DbTableName = SchemaNames.user + "." + TableNames.Users;

        public User()
        {
            UserRoles = new HashSet<UserRole>();
            Messages = new HashSet<Message>();
            MessageRecipients = new HashSet<MessageRecipient>();
        }

        [Required]
        public int Id { get; set; }

        [Required]
        [MaxLength(255)]
        public string UserName { get; set; }

        public string PasswordHash { get; set; }

        public string PhoneNumber { get; set; }

        [MaxLength(100)]
        public string FirstName { get; set; }

        [MaxLength(100)]
        public string LastName { get; set; }

        [Required]
        public DateTime Created { get; set; }

        [Required]
        public DateTime Updated { get; set; }

        [Required]
        public bool IsEmailConfirmed { get; set; }

        [MaxLength(255)]
        public string ConfirmationToken { get; set; }

        public virtual UserDetail UserDetail { get; set; }

        public virtual ICollection<Message> Messages { get; set; }

        public virtual ICollection<MessageRecipient> MessageRecipients { get; set; }


        public virtual ICollection<UserRole> UserRoles { get; set; }
    }
}
