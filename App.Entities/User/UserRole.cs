﻿using App.Common.Constants.Database;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Entities
{
    [Table(DbTableName)]
    public class UserRole : IEntity<int>
    {
        public const string DbTableName = SchemaNames.user + "." + TableNames.UserRoles;
        public const string DbRoleIdName = "RoleId";
        public const string DbUserIdName = "UserId";

        public int Id { get; set; }

        public virtual int RoleId { get; set; }

        public virtual int UserId { get; set; }

        public virtual Role Role { get; set; }
        public virtual User User { get; set; }
    }
}
