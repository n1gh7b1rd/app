﻿using Microsoft.AspNet.Identity;
using App.Common.Constants.Database;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Entities
{
    [Table(DbTableName)]
    public class Role : IEntity<int>, IRole<int>
    {
        public const string DbTableName = SchemaNames.user + "." + TableNames.Roles;

        public Role()
        {
            UserRoles = new HashSet<UserRole>();
        }
        public Role(string name)
        {
            Name = name;
            UserRoles = new HashSet<UserRole>();
        }
        public Role(int id, string name)
        {
            Id = id;
            Name = name;
            UserRoles = new HashSet<UserRole>();
        }

        [Required]
        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [MaxLength(50)]
        public string Name { get; set; }

        //public virtual ICollection<User> Users { get; set; }
        public virtual ICollection<UserRole> UserRoles { get; set; }


    }
}
