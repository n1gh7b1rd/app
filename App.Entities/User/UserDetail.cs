﻿namespace App.Entities
{
    using App.Common.Constants.Database;
    using System;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;
    using System.Collections.Generic;

    [Table(DbTableName)]
    public partial class UserDetail : IEntity<int>
    {
        public const string DbTableName = SchemaNames.user + "." + TableNames.UserDetails;

        [Key, DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [StringLength(500)]
        public string PicturePath { get; set; }

        [StringLength(500)]
        public string Nickname { get; set; }

        [Column(TypeName = "date")]
        public DateTime? DateOfBirth { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }


        public virtual User User { get; set; }

    }
}
