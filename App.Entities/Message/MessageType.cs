﻿namespace App.Entities
{
    using Common.Constants.Database;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(DbTableName)]
    public partial class MessageType : IEntity<int>
    {
        public const string DbTableName = SchemaNames.message + "." + TableNames.MessageTypes;

        public MessageType()
        {
            Messages = new HashSet<Message>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<Message> Messages { get; set; }
    }
}
