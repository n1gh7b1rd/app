﻿namespace App.Entities
{
    using App.Common.Constants.Database;
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(SchemaNames.message + "." + TableNames.Messages)]
    public partial class Message : IEntity<int>
    {
        public Message()
        {
            MessageRecipients = new HashSet<MessageRecipient>();
        }

        public int Id { get; set; }

        public int SenderId { get; set; }

        [StringLength(500)]
        public string MessageText { get; set; }

        [Required]
        [StringLength(256)]
        public string ConversationId { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public int MessageTypeId { get; set; }

        public virtual MessageType MessageType { get; set; }

        public virtual User Sender { get; set; }
        public virtual ICollection<MessageRecipient> MessageRecipients { get; set; }
    }
}
