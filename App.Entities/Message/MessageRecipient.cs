﻿namespace App.Entities
{
    using Common.Constants.Database;
    using System;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(DbTableName)]
    public partial class MessageRecipient : IEntity<int>
    {
        public const string DbTableName = SchemaNames.message + "." + TableNames.MessageRecipients;

        public int Id { get; set; }

        public int RecipientId { get; set; }

        public int MessageId { get; set; }

        public int MessageStatusTypeId { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public virtual MessageStatusType MessageStatusType { get; set; }

        public virtual User Recipient { get; set; }

        public virtual Message Message { get; set; }
    }
}
