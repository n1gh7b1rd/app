﻿namespace App.Entities
{
    using Common.Constants.Database;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;
    using System.ComponentModel.DataAnnotations.Schema;

    [Table(DbTableName)]
    public partial class MessageStatusType : IEntity<int>
    {
        public const string DbTableName = SchemaNames.message + "." + TableNames.MessageStatusTypes;

        public MessageStatusType()
        {
            MessageRecipients = new HashSet<MessageRecipient>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int Id { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<MessageRecipient> MessageRecipients { get; set; }
    }
}
