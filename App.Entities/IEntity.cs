﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Entities
{
    public interface IEntity<TKey> where TKey : struct //TKey will be a primitive type
    {
        TKey Id { get; set; }
    }
}
