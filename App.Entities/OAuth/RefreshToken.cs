﻿
using App.Common.Constants.Database;
using App.Entities;
using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Entities
{
    [Table(DbTableName)]
    public class RefreshToken : IEntity<int>
    {
        public const string DbTableName = SchemaNames.oauth + "." + TableNames.RefreshTokens;

        public int Id { get; set; }

        [Required]
        public string Token { get; set; }

        [Required]
        [StringLength(50)]
        public string UserName { get; set; }

        [Required]
        public int ClientAppId { get; set; }

        public DateTime IssuedUtc { get; set; }

        public DateTime ExpiresUtc { get; set; }

        [Required]
        public string ProtectedTicket { get; set; }

        public virtual ClientApp ClientApp { get; set; }
    }
}
