﻿using App.Common.Constants.Database;
using App.Entities;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace App.Entities
{
    [Table(DbTableName)]
    public class ClientApp : IEntity<int>
    {
        public const string DbTableName = SchemaNames.oauth + "." + TableNames.ClientApps;

        public ClientApp()
        {
            RefreshTokens = new HashSet<RefreshToken>();
        }

        public int Id { get; set; }

        [Required]
        public string Secret { get; set; }

        [Required]
        [StringLength(100)]
        public string Name { get; set; }

        public int ClientApplicationTypeId { get; set; }

        public bool Active { get; set; }

        public int RefreshTokenLifeTime { get; set; }

        [MaxLength(100)]
        [Required]
        public string AllowedOrigin { get; set; }

        public virtual ICollection<RefreshToken> RefreshTokens { get; set; }

        public virtual ClientApplicationType ClientApplicationType { get; set; }
    }
}
