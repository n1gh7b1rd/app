﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using App.Entities;
using App.Common.Constants.Database;

namespace App.Entities
{
    [Table(DbTableName)]

    public class ClientApplicationType : IEntity<int>
    {
        public const string DbTableName = SchemaNames.oauth + "." + TableNames.ClientApplicationTypes;

        public ClientApplicationType()
        {
            ClientApps = new HashSet<ClientApp>();
        }

        public int Id { get; set; }

        [StringLength(50)]
        public string Name { get; set; }

        public virtual ICollection<ClientApp> ClientApps { get; set; }

    }
}
