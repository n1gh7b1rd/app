﻿namespace App.Data.Migrations
{
    using Common.Enums;
    using Common.Enums.Role;
    using Entities;
    using System;
    using System.Collections.Generic;
    using System.Data.Entity.Migrations;
    using Common.Enums.Extensions;
    using System.Linq;
    using Common.Constants;

    internal sealed class Configuration : DbMigrationsConfiguration<AppDbContext>
    {
        public Configuration()
        {
            //AutomaticMigrationsEnabled = true;
        }
        //  This method will be called after migrating to the latest version.
        protected override void Seed(AppDbContext context)
        {
            SeedRoles(context);
            SeedClientAppTypes(context);
            SeedAdminUser(context);
            SeedUser(context);

            context.SaveChanges();
        }


        private void SeedRoles(AppDbContext context)
        {
            var roles = Enum.GetValues(typeof(RoleEnum));
            foreach (RoleEnum role in roles)
            {
                context.Roles.AddOrUpdate(new Role((int)role, role.ToString()));
            }
        }


        private void SeedClientAppTypes(AppDbContext context)
        {
            var appTypeWeb = new ClientApplicationType { Id = (int)ClientApplicationTypesEnum.Web, Name = ClientApplicationTypesEnum.Web.ToString() };
            var appTypeMobile = new ClientApplicationType { Id = (int)ClientApplicationTypesEnum.Mobile, Name = ClientApplicationTypesEnum.Mobile.ToString() };
            var appTypeNotSpecified = new ClientApplicationType { Id = (int)ClientApplicationTypesEnum.NotSpecified, Name = ClientApplicationTypesEnum.NotSpecified.ToString() };

            context.ClientApplicationTypes.AddOrUpdate(appTypeWeb);
            context.ClientApplicationTypes.AddOrUpdate(appTypeMobile);
            context.ClientApplicationTypes.AddOrUpdate(appTypeNotSpecified);
        }

        private void SeedUser(AppDbContext context)
        {
            var name = "User";
            var user = new User
            {
                UserName = "user-App-mm@yopmail.com",
                FirstName = name,
                LastName = name,
                PasswordHash = "AAzPcrv0KEBHLEkjnCnx4h8861MGmqgj1a9oeYIg6kiFAuesQ4W3Ak0sSP9RP8KlGg==",
                ConfirmationToken = "",
                IsEmailConfirmed = true,
                PhoneNumber = "+2345678",
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
                UserRoles = new List<UserRole>
                {
                    new UserRole() { Id = 2, UserId = 2, RoleId = (int)RoleEnum.User }
                }
            };
            var userDetail = new UserDetail()
            {
                PicturePath = "default-2.png",
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
            };
            user.UserDetail = userDetail;
            context.Users.AddOrUpdate(x => new { x.UserName }, user);
        }

        private void SeedAdminUser(AppDbContext context)
        {
            var admin = "Admin";
            var user = new User
            {
                UserName = "admin-App-mm@yopmail.com",
                FirstName = admin,
                LastName = admin,
                PasswordHash = "AAYWmunxT2uQW4PzVy3ApKvVMr3AHhmqM1RIVs1NJhO0CCq5JQSHPUkFqnaIBF/oBg==",
                ConfirmationToken = "",
                IsEmailConfirmed = true,
                PhoneNumber = "+123456",
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
                UserRoles = new List<UserRole>
                {
                    new UserRole() { Id = 1, UserId = 1, RoleId = (int)RoleEnum.Admin }
                }
            };
            var userDetail = new UserDetail()
            {
                PicturePath = "default-1.png",
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow,
            };
            user.UserDetail = userDetail;
            context.Users.AddOrUpdate(x => new { x.UserName }, user);
        }

        private void SeedClientApps(AppDbContext context)
        {
            var webApp = new ClientApp
            {
                Id = 1,
                Active = true,
                AllowedOrigin = "*", //TODO: in production db this should be equal to the web app domain
                ClientApplicationTypeId = 1,
                Name = "AppWeb",
                RefreshTokenLifeTime = 7200,
                Secret = "034c3322-7ac1-4acb-ab93-bfed66047abq"
            };

            context.ClientApps.AddOrUpdate(webApp);
        }
    }
}
