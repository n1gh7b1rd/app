namespace App.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "oauth.ClientApplicationTypes",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "oauth.ClientApps",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Secret = c.String(nullable: false),
                        Name = c.String(nullable: false, maxLength: 100),
                        ClientApplicationTypeId = c.Int(nullable: false),
                        Active = c.Boolean(nullable: false),
                        RefreshTokenLifeTime = c.Int(nullable: false),
                        AllowedOrigin = c.String(nullable: false, maxLength: 100),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("oauth.ClientApplicationTypes", t => t.ClientApplicationTypeId)
                .Index(t => t.ClientApplicationTypeId);
            
            CreateTable(
                "oauth.RefreshTokens",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Token = c.String(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 50),
                        ClientAppId = c.Int(nullable: false),
                        IssuedUtc = c.DateTime(nullable: false),
                        ExpiresUtc = c.DateTime(nullable: false),
                        ProtectedTicket = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("oauth.ClientApps", t => t.ClientAppId)
                .Index(t => t.ClientAppId);
            
            CreateTable(
                "message.MessageRecipients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RecipientId = c.Int(nullable: false),
                        MessageId = c.Int(nullable: false),
                        MessageStatusTypeId = c.Int(nullable: false),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("message.Messages", t => t.MessageId)
                .ForeignKey("user.Users", t => t.RecipientId)
                .ForeignKey("message.MessageStatusTypes", t => t.MessageStatusTypeId)
                .Index(t => t.RecipientId)
                .Index(t => t.MessageId)
                .Index(t => t.MessageStatusTypeId);
            
            CreateTable(
                "message.Messages",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        SenderId = c.Int(nullable: false),
                        MessageText = c.String(maxLength: 500),
                        ConversationId = c.String(nullable: false, maxLength: 256),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        MessageTypeId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("message.MessageTypes", t => t.MessageTypeId)
                .ForeignKey("user.Users", t => t.SenderId)
                .Index(t => t.SenderId)
                .Index(t => t.MessageTypeId);
            
            CreateTable(
                "message.MessageTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "user.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false, maxLength: 255),
                        PasswordHash = c.String(),
                        PhoneNumber = c.String(),
                        FirstName = c.String(maxLength: 100),
                        LastName = c.String(maxLength: 100),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                        IsEmailConfirmed = c.Boolean(nullable: false),
                        ConfirmationToken = c.String(maxLength: 255),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "user.UserDetails",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        PicturePath = c.String(maxLength: 500),
                        Nickname = c.String(maxLength: 500),
                        DateOfBirth = c.DateTime(storeType: "date"),
                        Created = c.DateTime(nullable: false),
                        Updated = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("user.Users", t => t.Id)
                .Index(t => t.Id);
            
            CreateTable(
                "user.UserRoles",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        RoleId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("user.Roles", t => t.RoleId)
                .ForeignKey("user.Users", t => t.UserId)
                .Index(t => t.RoleId)
                .Index(t => t.UserId);
            
            CreateTable(
                "user.Roles",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "message.MessageStatusTypes",
                c => new
                    {
                        Id = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 50),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("message.MessageRecipients", "MessageStatusTypeId", "message.MessageStatusTypes");
            DropForeignKey("user.UserRoles", "UserId", "user.Users");
            DropForeignKey("user.UserRoles", "RoleId", "user.Roles");
            DropForeignKey("user.UserDetails", "Id", "user.Users");
            DropForeignKey("message.Messages", "SenderId", "user.Users");
            DropForeignKey("message.MessageRecipients", "RecipientId", "user.Users");
            DropForeignKey("message.Messages", "MessageTypeId", "message.MessageTypes");
            DropForeignKey("message.MessageRecipients", "MessageId", "message.Messages");
            DropForeignKey("oauth.ClientApps", "ClientApplicationTypeId", "oauth.ClientApplicationTypes");
            DropForeignKey("oauth.RefreshTokens", "ClientAppId", "oauth.ClientApps");
            DropIndex("user.UserRoles", new[] { "UserId" });
            DropIndex("user.UserRoles", new[] { "RoleId" });
            DropIndex("user.UserDetails", new[] { "Id" });
            DropIndex("message.Messages", new[] { "MessageTypeId" });
            DropIndex("message.Messages", new[] { "SenderId" });
            DropIndex("message.MessageRecipients", new[] { "MessageStatusTypeId" });
            DropIndex("message.MessageRecipients", new[] { "MessageId" });
            DropIndex("message.MessageRecipients", new[] { "RecipientId" });
            DropIndex("oauth.RefreshTokens", new[] { "ClientAppId" });
            DropIndex("oauth.ClientApps", new[] { "ClientApplicationTypeId" });
            DropTable("message.MessageStatusTypes");
            DropTable("user.Roles");
            DropTable("user.UserRoles");
            DropTable("user.UserDetails");
            DropTable("user.Users");
            DropTable("message.MessageTypes");
            DropTable("message.Messages");
            DropTable("message.MessageRecipients");
            DropTable("oauth.RefreshTokens");
            DropTable("oauth.ClientApps");
            DropTable("oauth.ClientApplicationTypes");
        }
    }
}
