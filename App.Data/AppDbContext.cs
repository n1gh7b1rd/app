﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Configuration;
using App.Entities;
using App.Common.Constants.Database;

namespace App.Data
{
    public class AppDbContext : DbContext
    {
        public Lazy<int?> CurrentUserId { get; set; }

        #region Message Schema
        public virtual DbSet<Message> Messages { get; set; }
        public virtual DbSet<MessageRecipient> MessageRecipients { get; set; }
        public virtual DbSet<MessageStatusType> MessageStatusTypes { get; set; }
        public virtual DbSet<MessageType> MessageTypes { get; set; }
        #endregion

        #region OAuth Schema
        public virtual DbSet<ClientApp> ClientApps { get; set; }
        public virtual DbSet<RefreshToken> RefreshTokens { get; set; }
        public virtual DbSet<ClientApplicationType> ClientApplicationTypes { get; set; }
        #endregion


        #region User Schema
        public virtual DbSet<User> Users { get; set; }
        public virtual DbSet<Role> Roles { get; set; }
        public virtual DbSet<UserRole> UserRoles { get; set; }
        public virtual DbSet<UserDetail> UserDetails { get; set; }
        #endregion

        public AppDbContext()
            : base(DatabaseConstants.DbConnectionName)
        {
            Configuration.ProxyCreationEnabled = true;
            Configuration.LazyLoadingEnabled = true;
        }

        public static AppDbContext Create()
        {
            return new AppDbContext();
        }

        public override DbSet<TEntity> Set<TEntity>()
        {
            return base.Set<TEntity>();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
            modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

            MessageSchema(modelBuilder);

            OAuthSchema(modelBuilder);

            UserSchema(modelBuilder);
        }


        private static void MessageSchema(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Message>()
                .HasMany(e => e.MessageRecipients)
                .WithRequired(e => e.Message)
                .HasForeignKey(e => e.MessageId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MessageType>()
                .HasMany(e => e.Messages)
                .WithRequired(e => e.MessageType)
                .HasForeignKey(e => e.MessageTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<MessageStatusType>()
                .HasMany(e => e.MessageRecipients)
                .WithRequired(e => e.MessageStatusType)
                .HasForeignKey(e => e.MessageStatusTypeId)
                .WillCascadeOnDelete(false);
        }

        private static void OAuthSchema(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<ClientApplicationType>()
                .HasMany(e => e.ClientApps)
                .WithRequired(e => e.ClientApplicationType)
                .HasForeignKey(e => e.ClientApplicationTypeId)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<ClientApp>()
                .HasMany(e => e.RefreshTokens)
                .WithRequired(rt => rt.ClientApp)
                .HasForeignKey(rt => rt.ClientAppId)
                .WillCascadeOnDelete(false);
        }

        private static void UserSchema(DbModelBuilder modelBuilder)
        {
            #region User
            //PK and FK UserID
            modelBuilder.Entity<UserDetail>()
               .HasKey(e => e.Id);
            //One-To-One relation
            modelBuilder.Entity<User>()
                .HasRequired(e => e.UserDetail)
                .WithRequiredPrincipal(e => e.User);

            modelBuilder.Entity<User>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.User)
                .HasForeignKey(e => e.UserId)
                .WillCascadeOnDelete(false);

            #endregion

            #region Role
            modelBuilder.Entity<Role>()
                .HasMany(e => e.UserRoles)
                .WithRequired(e => e.Role)
                .HasForeignKey(e => e.RoleId)
                .WillCascadeOnDelete(false);
            #endregion

        }
    }
}
