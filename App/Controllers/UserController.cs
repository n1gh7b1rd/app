﻿using Microsoft.AspNet.Identity;
using App.API.Attributes;
using App.Common.Enums.Role;
using App.Core.Services;
using App.Models.User;
using System.Threading.Tasks;
using System.Web.Http;
using App.Entities;
using System;
using App.Common.Utility;
using App.Core.Services.Files;
using App.Utility;
using App.Common.Enums;

namespace App.API.Controllers
{
    [RoutePrefix("user")]
    public class UserController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IScheduler _scheduler;
        private readonly IFileService _fileService;
        public UserController(
            IUserService userService,
            IScheduler scheduler,
            IFileService fileService)
        {
            _userService = userService;
            _scheduler = scheduler;
            _fileService = fileService;
        }

        [Authorize]
        [HttpGet]
        [Route("getUserInfo")]
        public async Task<IHttpActionResult> GetUserInfo()
        {
            int userId = int.Parse(User.Identity.GetUserId());
            var info = await _userService.GetUserProfileInformation(userId);
            return Ok(info);
        }

        [Authorize]
        [HttpGet]
        [Route("getUserSettings")]
        public async Task<IHttpActionResult> GetUserSettings()
        {
            int userId = int.Parse(User.Identity.GetUserId());
            UserSettingsDTO info = await _userService.GetUserSettings(userId);
            return Ok(info);
        }


        [AppAuthorize(Roles = new RoleEnum[] { RoleEnum.Admin,  RoleEnum.User })]
        [HttpPost]
        [Route("changePassword")]
        public async Task<IHttpActionResult> ChangePassword(string currentPassword, string newPassword)
        {
            int userId = int.Parse(User.Identity.GetUserId());
            IdentityResult result = await UserManager.ChangePasswordAsync(userId, currentPassword, newPassword);
            if (!result.Succeeded)
            {
                return AppBadRequest(result.Errors);
            }

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("updateUserInfo")]
        public async Task<IHttpActionResult> UpdateUserInfo(UserInfoDTO userInfo)
        {
            int userId = int.Parse(User.Identity.GetUserId());
            if (!ModelState.IsValid)
            {
                return AppBadRequest(GetErrorListFromModelState());
            }

            await _userService.UpdateUserInfo(userId, userInfo);
            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("updateUserAndDetails")]
        public async Task<IHttpActionResult> UpdateUser(UserAndDetailsDTO userInfo)
        {
            int userId = int.Parse(User.Identity.GetUserId());
            if (!ModelState.IsValid)
            {
                return AppBadRequest(GetErrorListFromModelState());
            }
            await _userService.UpdateUserAndDetails(userId, userInfo);
            return Ok();
        }

        [AppAuthorize(Roles = new RoleEnum[] { RoleEnum.Admin, RoleEnum.User })]
        [HttpPost]
        [Route("updateUserDetails")]
        public async Task<IHttpActionResult> UpdateUserDetails(UserDetailsDTO data)
        {
            int userId = int.Parse(User.Identity.GetUserId());
            if (!ModelState.IsValid)
            {
                return AppBadRequest(GetErrorListFromModelState());
            }

            await _userService.UpdateUserDetails(userId, data);
            return Ok();
        }


        [HttpPost]
        [Route("signup")]
        public async Task<IHttpActionResult> SignUp(UserSignUpModel model)
        {
            var appType = GetAppType();

            if (!ModelState.IsValid)
            {
                return AppBadRequest(GetErrorListFromModelState());
            }

            var user = new User()
            {
                UserName = model.UserName,
                FirstName = model.FirstName,
                LastName = model.LastName,
                PhoneNumber = model.PhoneNumber,
                IsEmailConfirmed = false,
                ConfirmationToken = Guid.NewGuid().ToString(),
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow
            };

            RoleEnum role = appType == ClientApplicationTypesEnum.Web ? RoleEnum.User : RoleEnum.Admin;//TODO

            user.UserRoles.Add(new UserRole() { RoleId = (int)role });
           
            var userDetail = new UserDetail()
            {
                PicturePath = _fileService.GetRandomDefaultPictureName(),
                Created = DateTime.UtcNow,
                Updated = DateTime.UtcNow
            };
            user.UserDetail = userDetail;

            IdentityResult addUserResult = await this.UserManager.CreateAsync(user, model.Password);
            if (addUserResult == null)
            {
                return InternalServerError();
            }
            if (!addUserResult.Succeeded)
            {
                return AppBadRequest(addUserResult.Errors);
            }

            return Ok();
        }

        [HttpGet]
        [Route("confirmEmail/{token}")]
        public async Task<IHttpActionResult> ConfirmEmail(string token)
        {
            try
            {
                await _userService.ConfirmRegistrationEmail(token);
            }
            catch (ArgumentNullException ex)
            {
                return AppBadRequest(ex.Message.InList());
            }
            catch (ArgumentException ex)
            {
                return AppBadRequest(ex.Message.InList());
            }

            return Ok();
        }

        [HttpPost]
        [Route("initPasswordRecovery")]
        public async Task<IHttpActionResult> InitPasswordRecovery(string email)
        {
            try
            {
                await _userService.InitPasswordRecovery(email);
            }
            catch (ArgumentNullException ex)
            {
                return AppBadRequest(ex.Message.InList());
            }
            catch (FormatException ex)
            {
                return AppBadRequest(ex.Message.InList());
            }

            return Ok();
        }

        [HttpPost]
        [Route("resetPassword/{token}")]
        public async Task<IHttpActionResult> ResetPassword(string token, string newPassword)
        {
            try
            {
                var isValidPassword = await UserManager.PasswordValidator.ValidateAsync(newPassword);
                if (!isValidPassword.Succeeded)
                {
                    return AppBadRequest("Invalid password".InList());
                }

                var passwordHash = UserManager.PasswordHasher.HashPassword(newPassword);
                await _userService.ResetPassword(token, passwordHash);
            }
            catch (ArgumentException ex)
            {
                return AppBadRequest(ex.Message.InList());

            }
            return Ok();
        }

        [Authorize]
        [HttpGet]
        [Route("getDefaultProfilePictures")]
        public IHttpActionResult GetDefaultPictures()
        {
            var namesUrlsMap = _fileService.GenerateDefaultProfilePicturesNamesUrlsMap();
            return Ok(namesUrlsMap);
        }

        [Authorize]
        [HttpPost]
        [Route("uploadProfilePicture")]
        //public IHttpActionResult UploadProfilePicture(HttpPostedFile pictureFile)
        public async Task<IHttpActionResult> UploadProfilePicture(UploadedFile file)
        {
            //TODO: use authorize attribute and rely on logged in user.
            int userId = int.Parse(User.Identity.GetUserId());
            try
            {
                //int userId = 1;
                await _userService.UploadProfilePicture(file, userId);
            }
            catch (ArgumentException ex)
            {
                return AppBadRequest(ex.Message.InList());
            }

            return Ok();
        }

        [Authorize]
        [HttpPost]
        [Route("setDefaultProfilePicture")]
        //public IHttpActionResult UploadProfilePicture(HttpPostedFile pictureFile)
        public async Task<IHttpActionResult> SetDefaultProfilePicture(string name)
        {
            int userId = int.Parse(User.Identity.GetUserId());
            try
            {
                await _userService.SetDefaultProfilePicture(name, userId);
            }
            catch (ArgumentException ex)
            {
                return AppBadRequest(ex.Message.InList());
            }

            return Ok();
        }
    }
}
