﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using App.API.Attributes;
using App.Common.Enums;
using App.Common.Enums.Extensions;
using App.Data;
using App.Models.ApiResult;
using App.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace App.API.Controllers
{
    public class BaseApiController : ApiController
    {
        private AppUserManager _userManager = null;
        protected AppUserManager UserManager
        {
            get
            {
                return _userManager ?? Request.GetOwinContext().GetUserManager<AppUserManager>();
            }
        }

        public BaseApiController()
        {
        }

        protected IHttpActionResult AppBadRequest(IEnumerable<string> errors = null)
        {
            ErrorResult result = new ErrorResult();
            result.Errors = errors;
            return Content(HttpStatusCode.BadRequest, result);
        }

        protected List<string> GetErrorListFromModelState()
        {
            List<string> errorList = new List<string>();
            foreach (var modelstates in ModelState.Values)
            {
                if (modelstates.Errors.Any(x => x.ErrorMessage != string.Empty))
                    errorList.AddRange(modelstates.Errors.Select(x => x.ErrorMessage));

                errorList.AddRange(modelstates.Errors.Select(x => x.Exception.Message));
            }

            return errorList;
        }

        protected ClientApplicationTypesEnum GetAppType()
        {
            ClientApplicationTypesEnum appType = ClientApplicationTypesEnum.NotSpecified;
            var appTypeHeader = HttpContext.Current.Request.Headers.Get("AppType");
            if (appTypeHeader != null)
            {
                appType = appTypeHeader.ToEnum<ClientApplicationTypesEnum>();
            }

            return appType;
        }

    }


}
