﻿using App.Core.Services;
using System.Web.Http;
using System.Collections.Generic;
using App.Core.Services.Files;

namespace App.API.Controllers
{
    [RoutePrefix("file")]
    public class FileController : BaseApiController
    {
        private readonly IUserService _userService;
        private readonly IFileService _fileService;
        public FileController(IUserService userService,
            IFileService fileService)
        {
            _userService = userService;
            _fileService = fileService;
        }

        [Authorize]
        [HttpGet]
        [Route("getFileUrl")]
        public IHttpActionResult GetFileUrl(string fileName)
        {
            string url = _fileService.GetFileUrl(fileName);
            return Ok(url);
        }

        [Authorize]
        [HttpGet]
        [Route("getFileUrls")]
        public IHttpActionResult GetFileUrls(IEnumerable<string> fileNames)
        {
            var urls = _fileService.GetFileUrls(fileNames);
            return Ok(urls);
        }

        [HttpGet]
        [Route("getTermsAndConditionsUrl")]
        public IHttpActionResult GetTermsAndConditions()
        {
            var url = _fileService.GetTermsAndConditionsUrl();
            return Ok(url);
        }

        [HttpGet]
        [Route("getPrivacyPolicyUrl")]
        public IHttpActionResult GetPrivacyPolicy()
        {
            var url = _fileService.GetPrivacyPolicyUrl();
            return Ok(url);
        }
    }
}
