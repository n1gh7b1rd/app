﻿using Microsoft.AspNet.Identity;
using App.API.Attributes;
using App.Common.Enums.Role;
using App.Common.Utility;
using App.Core.Services;
using App.Models;
using System;
using System.Threading.Tasks;
using System.Web.Http;

namespace App.API.Controllers
{
    [RoutePrefix("Report")]
    public class ReportController : BaseApiController
    {
        private readonly IReportService _reportService;

        public ReportController(IReportService reportService)
        {
            _reportService = reportService;
        }

        [AppAuthorize(Roles = new RoleEnum[] { RoleEnum.Admin })]
        [HttpGet]
        [Route("scheduleReport")]
        public async Task<IHttpActionResult> ScheduleReport()
        {
            int userId = int.Parse(User.Identity.GetUserId());

            var result = await _reportService.RunReport("test-" + DateTime.UtcNow.ToString("MM-dd-yyyy"), "select * from [user].Users;", false);
            var result2 = await _reportService.ScheduleDailyRecurringReport("test", "select * from [user].Users;", false);

            return Ok();
        }
    }
}
