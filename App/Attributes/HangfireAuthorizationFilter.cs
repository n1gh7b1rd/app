﻿using Hangfire.Dashboard;
using Microsoft.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Hangfire.Annotations;
using App.Common.Enums;
using App.Common.Enums.Role;

namespace App.API.Attributes
{
    public class HangfireAuthorizationFilter : IDashboardAuthorizationFilter
    {
        public bool Authorize([NotNull] DashboardContext context)
        {
            bool isAdmin = HttpContext.Current.User.IsInRole(RoleEnum.Admin.ToString());
            //return isAdmin;
            //TODO:
            return true;
        }
    }
}