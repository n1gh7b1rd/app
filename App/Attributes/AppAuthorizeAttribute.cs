﻿using App.Security;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using App.Common.Enums;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Controllers;
using App.Common.Enums.Role;

namespace App.API.Attributes
{
    public class AppAuthorizeAttribute : AuthorizeAttribute
    {
        public new RoleEnum[] Roles { get; set; }

        public override void OnAuthorization(HttpActionContext actionContext)
        {
            if (HttpContext.Current.User.Identity.IsAuthenticated)
            {
                var userId = HttpContext.Current.User.Identity.GetUserId();
                var _userManager = HttpContext.Current.GetOwinContext().Get<AppUserManager>();

                AuthorizeByRoles(Roles, userId, _userManager, actionContext);
            }
            base.OnAuthorization(actionContext);
        }

        private void AuthorizeByRoles(IEnumerable<RoleEnum> roles, string userId, AppUserManager userManager, HttpActionContext actionContext)
        {
            bool authorized = false;
            foreach (var role in roles)
            {
                if (userManager.IsInRole(int.Parse(userId), role.ToString()))
                {
                    authorized = true;
                    break;
                }
            }

            if (!authorized)
            {
                actionContext.Response = new HttpResponseMessage(System.Net.HttpStatusCode.Forbidden);
            }
        }
    }
}