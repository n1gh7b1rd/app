﻿using Hangfire.Common;
using Hangfire.Server;

namespace App.API.App_Start
{
    public class AppChildContainerPerJobFilterAttribute : JobFilterAttribute, IServerFilter
    {
        public AppChildContainerPerJobFilterAttribute(UnityJobActivator unityJobActivator)
        {
            UnityJobActivator = unityJobActivator;
        }

        public UnityJobActivator UnityJobActivator { get; set; }

        public void OnPerformed(PerformedContext filterContext)
        {
            UnityJobActivator.DisposeChildContainer();
        }

        public void OnPerforming(PerformingContext filterContext)
        {
            UnityJobActivator.CreateChildContainer();
        }
    }
}