using App.Core.Logging;
using App.Data;
using App.Logging;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Compilation;

namespace App.API.App_Start
{
    public static class UnityConfig
    {
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            container.RegisterType<AppDbContext>(new HierarchicalLifetimeManager());
            RegisterRepositories(container);
            RegisterLogging(container);
            RegisterServices(container);
            return container;
        });

        /// <summary>
        /// Make sure this is disposed after usage.
        /// </summary>
        /// <returns></returns>
        public static IUnityContainer GetConfiguredContainerChild()
        {
            return container.Value.CreateChildContainer();
        }

        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        private static void RegisterRepositories(IUnityContainer container)
        {
            var classes = AllClasses.FromAssemblies(
             BuildManager.GetReferencedAssemblies().Cast<Assembly>());

            var repositoryClasses = classes
                .Where(t => t.Namespace.StartsWith("App.Repositories"));

            container.RegisterTypes(repositoryClasses,
            WithMappings.FromMatchingInterface,
            WithName.Default,
            WithLifetime.Hierarchical);
        }
        private static void RegisterLogging(IUnityContainer container)
        {
            container.RegisterType(typeof(ILogger), typeof(Log4NetLogger), new HierarchicalLifetimeManager());
        }
        private static void RegisterServices(IUnityContainer container)
        {
            var classes = AllClasses.FromAssemblies(
             BuildManager.GetReferencedAssemblies().Cast<Assembly>());
            var serviceClasses = classes.Where(t => t.Namespace.StartsWith("App.Services"));

            container.RegisterTypes(serviceClasses,
                WithMappings.FromMatchingInterface,
                WithName.Default,
                WithLifetime.Hierarchical);
        }
    }
}