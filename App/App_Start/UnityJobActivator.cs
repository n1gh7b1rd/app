﻿using System;
using Hangfire;
using Microsoft.Practices.Unity;

namespace App.API.App_Start
{
    public class UnityJobActivator : JobActivator
    {
        [ThreadStatic]
        private static IUnityContainer childContainer;
        public IUnityContainer Container { get; set; }


        public UnityJobActivator(IUnityContainer container)
        {
            Container = container;
        }

        public override object ActivateJob(Type jobType)
        {
            return childContainer.Resolve(jobType);
        }

        public void CreateChildContainer()
        {
            childContainer = Container.CreateChildContainer();
        }

        public void DisposeChildContainer()
        {
            childContainer.Dispose();
            childContainer = null;
        }
    }
}