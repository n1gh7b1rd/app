﻿using App.Utility;
using App.API.Attributes;
using App.Security.OAuth;
using Unity.WebApi;
using Microsoft.Owin.Security.OAuth;
using App.Security;
using Microsoft.AspNet.Identity.Owin;
using App.API.App_Start;
using Microsoft.Owin;
using Microsoft.Owin.Cors;
using Owin;
using App.Data;
using Swashbuckle.Application;
using System;
using System.Net.Http;
using System.IO;
using System.Reflection;
using System.Web.Http;
using Microsoft.Practices.Unity;
using System.Web.Http.ExceptionHandling;
using App.Handlers;
using Microsoft.AspNet.Identity;
using App.Entities;
using System.Net.Http.Formatting;
using System.Linq;
using Newtonsoft.Json.Serialization;
using Hangfire;
using App.Common.Constants.Database;
using System.Configuration;
using Hangfire.Common;
using Newtonsoft.Json;

[assembly: log4net.Config.XmlConfigurator(ConfigFile = "Web.config", Watch = true)]
namespace App.API
{
    public class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            HttpConfiguration httpConfig = System.Web.Http.GlobalConfiguration.Configuration;

            app.UseCors(CorsOptions.AllowAll);

            ConfigureSwagger(httpConfig);

            ConfigureOAuthTokenGeneration(app);

            ConfigureFormatters(httpConfig);

            RegisterRoutes(httpConfig);

            ConfigureHangfire(app);

            ConfigureDependencyResolver(httpConfig);

            ConfigureLogging(httpConfig);

            app.UseWebApi(httpConfig);

            httpConfig.EnsureInitialized();
        }

        private void ConfigureFormatters(HttpConfiguration httpConfig)
        {
            httpConfig.Formatters.Add(new UploadMultipartMediaTypeFormatter());
        }

        private static void ConfigureHangfire(IAppBuilder app)
        {
            Hangfire.GlobalConfiguration.Configuration.UseSqlServerStorage(DatabaseConstants.DbConnectionName);

            /* Configure Hangfire to use Unity for Dependency Injection.
             * Also creates JobFilterAttribute to provide Child Unity Container per Job. (Required for async calls to the database) */
            UnityJobActivator unityJobActivator = new UnityJobActivator(UnityConfig.GetConfiguredContainer());
            JobActivator.Current = unityJobActivator;
            GlobalJobFilters.Filters.Add(new AppChildContainerPerJobFilterAttribute(unityJobActivator));

            JobHelper.SetSerializerSettings(new JsonSerializerSettings { TypeNameHandling = TypeNameHandling.All, CheckAdditionalContent = true, MaxDepth = int.MaxValue });

            app.UseHangfireDashboard("/hangfire", new DashboardOptions
            {
                Authorization = new[] { new HangfireAuthorizationFilter() }
            });

            app.UseHangfireServer();
        }

        private void ConfigureSwagger(HttpConfiguration httpConfig)
        {
            //httpConfig.EnableSwagger(c => c.SingleApiVersion("v0", "APP API"))
            //.EnableSwaggerUi();
            httpConfig
                .EnableSwagger(c =>
                {
                    c.SingleApiVersion("v2", "APP API");
                    c.RootUrl(req =>
                                req.RequestUri.GetLeftPart(UriPartial.Authority) +
                                req.GetRequestContext().VirtualPathRoot.TrimEnd('/'));
                })
                .EnableSwaggerUi(c =>
                {
                    Assembly exAssemb = Assembly.GetExecutingAssembly();
                    c.InjectJavaScript(exAssemb, "App.API.scripts.SwashbuckleUIAddons.js");
                });
        }

        private void ConfigureOAuthTokenGeneration(IAppBuilder app)
        {
            // Configure the db context and user manager to use a single instance per request
            app.CreatePerOwinContext(AppDbContext.Create);
            app.CreatePerOwinContext(() => UnityConfig.GetConfiguredContainerChild());
            app.CreatePerOwinContext<AppUserManager>(CreateUserManager);

            OAuthAuthorizationServerOptions OAuthServerOptions = new OAuthAuthorizationServerOptions()
            {
                AllowInsecureHttp = true,
                TokenEndpointPath = new PathString("/token"),
                AccessTokenExpireTimeSpan = TimeSpan.FromMinutes(11000), //TimeSpan.FromMinutes(int.Parse(ConfigurationManager.AppSettings["AccessTokenExpirationMinutes"])),
                Provider = new AppOAuthProvider(() => UnityConfig.GetConfiguredContainerChild()),
                RefreshTokenProvider = new AppRefreshTokenProvider(() => UnityConfig.GetConfiguredContainerChild())
            };

            // Token Generation
            app.UseOAuthAuthorizationServer(OAuthServerOptions);
            app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions());
        }

        private AppUserManager CreateUserManager(IdentityFactoryOptions<AppUserManager> options, IOwinContext context)
        {
            var container = context.Get<IUnityContainer>();
            var appDbContext = context.Get<AppDbContext>();
            container.RegisterInstance(appDbContext, new HierarchicalLifetimeManager());
            container.RegisterType<IUserStore<User, int>, AppUserStore>(new HierarchicalLifetimeManager());
            container.RegisterType<AppUserManager>(new HierarchicalLifetimeManager());

            var _userManager = container.Resolve<AppUserManager>();
            return _userManager;
        }

        private void RegisterRoutes(HttpConfiguration httpConfig)
        {
            // json formatter
            var jsonFormatter = httpConfig.Formatters.OfType<JsonMediaTypeFormatter>().First();
            jsonFormatter.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();

            // Web API routes
            httpConfig.MapHttpAttributeRoutes();

            // Requests made to the default route will be redirected to the swagger page
            httpConfig.Routes.MapHttpRoute(
                name: "swagger_root",
                routeTemplate: "",
                defaults: null,
                constraints: null,
                handler: new RedirectHandler((message => message.RequestUri.ToString()), "swagger")
            );

            // Default api route
            httpConfig.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }

        private void ConfigureDependencyResolver(HttpConfiguration httpConfig)
        {
            var dependencyResolverContainer = UnityConfig.GetConfiguredContainer();
            httpConfig.DependencyResolver = new UnityDependencyResolver(dependencyResolverContainer);
        }

        private void ConfigureLogging(HttpConfiguration httpConfig)
        {
            log4net.Config.XmlConfigurator.Configure(new FileInfo(System.Web.Hosting.HostingEnvironment.MapPath("~/Web.config")));

            httpConfig.Services.Replace(typeof(IExceptionHandler), new AppExceptionHandler());
        }



    }
}