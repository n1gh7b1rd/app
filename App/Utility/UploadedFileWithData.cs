﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading.Tasks;
using System.Web;

namespace App.Utility
{
    public class UploadedFileWithData : UploadedFile
    {
        private readonly MediaTypeFormatter formatter;
        private HttpContent content;
        private IFormatterLogger formatterLogger;

        public UploadedFileWithData(Stream stream, string contentType, string fileName, MediaTypeFormatter formatter, HttpContent content, IFormatterLogger formatterLogger) :
            base(stream, contentType, fileName)
        {
            this.formatter = formatter;
            this.content = content;
            this.formatterLogger = formatterLogger;
        }

        public async Task<T> GetData<T>()
        {
            var stream = await content.ReadAsStreamAsync();
            var dto = (T)await formatter.ReadFromStreamAsync(typeof(T), stream, content, formatterLogger);
            return dto;
        }
    }
}