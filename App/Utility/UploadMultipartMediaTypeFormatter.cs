﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace App.Utility
{
    public class UploadMultipartMediaTypeFormatter : MediaTypeFormatter
    {
        public UploadMultipartMediaTypeFormatter()
        {
            SupportedMediaTypes.Add(new MediaTypeHeaderValue("multipart/form-data"));
        }

        public override bool CanReadType(Type type)
        {
            return true;
        }

        public override bool CanWriteType(Type type)
        {
            return false;
        }

        public async override Task<object> ReadFromStreamAsync(Type type, Stream readStream, HttpContent content, IFormatterLogger formatterLogger)
        {
            var provider = await content.ReadAsMultipartAsync();

            UploadedFile file = null;

            HttpContent fileContent = provider.Contents.FirstOrDefault(x => x.Headers.ContentType.MediaType != "application/json");
            if (fileContent != null)
            {
                Stream stream = await fileContent.ReadAsStreamAsync();
                string mediaType = fileContent.Headers.ContentType.MediaType;
                string fileName = fileContent.Headers.ContentDisposition.FileName.Trim('"');

                var jsonData = provider.Contents.FirstOrDefault(x => x.Headers.ContentType.MediaType == "application/json");
                if (jsonData != null)
                {
                    var deserializedJson = new Dictionary<string, object>();
                    var jsonFormatter = GlobalConfiguration.Configuration.Formatters.JsonFormatter;
                    file = new UploadedFileWithData(stream, mediaType, fileName, jsonFormatter, jsonData, formatterLogger);
                }
                else
                {
                    file = new UploadedFile(stream, mediaType, fileName);
                }
            }

            return file;
        }
    }
}