﻿using App.Core.Logging;
using System.Web.Http.ExceptionHandling;
using Microsoft.Practices.Unity;
using App.API.App_Start;

namespace App.Handlers
{
    public class AppExceptionHandler : ExceptionHandler
    {
        public override void Handle(ExceptionHandlerContext context)
        {
            LogException(context);
        }

        private static void LogException(ExceptionHandlerContext context)
        {
            using (var container = UnityConfig.GetConfiguredContainerChild())
            {
                var _logger = container.Resolve<ILogger>();
                _logger.WriteError(context.Exception.Message, context.Exception);
            }
        }
    }
}