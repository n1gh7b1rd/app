﻿(function () {
    if (!window.CustomSwaggerUILoaded) {
        var basicAuthUI =
            '<div class="input"><input placeholder="username" id="input_username" name="username" type="text" size="10"></div>' +
            '<div class="input"><input placeholder="password" id="input_password" name="password" type="password" size="10"></div>' +
            '<div class="input"><a id="login" class="header__btn" href="#" data-sw-translate="">Login</a></div>';
        $(basicAuthUI).insertBefore('#api_selector div.input:last-child');
        $("#input_apiKey").hide();

        $('#login').click(function () {
            var pathArray = window.location.pathname.split('/');
            var swaggerPathIndex = pathArray.indexOf("swagger");
            var basePath = pathArray[swaggerPathIndex - 1];
            var baseUrl = window.location.protocol + "//" + window.location.host + "/" + basePath;

            var username = encodeURIComponent($('#input_username').val());
            var password = $('#input_password').val();
            $.ajax({
                url: baseUrl + "/token",
                type: "post",
                contenttype: 'x-www-form-urlencoded',
                data: "grant_type=password&username=" + username + "&password=" + password + "&client_id=popsWeb",
                success: function (response) {
                    var bearerToken = 'Bearer ' + response.access_token;
                    window.swaggerUi.api.clientAuthorizations.add('Authorization', new SwaggerClient.ApiKeyAuthorization('Authorization', bearerToken, 'header'));
                    window.swaggerUi.api.clientAuthorizations.remove("api_key");
                    alert("Login successfull");
                },
                error: function (xhr, ajaxoptions, thrownerror) {
                    alert("Login failed!");
                }
            });
        });
        window.CustomSwaggerUILoaded = true;
    }
})();

