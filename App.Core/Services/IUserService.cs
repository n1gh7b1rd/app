﻿using App.Entities;
using App.Models.User;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web;
using System.IO;

namespace App.Core.Services
{
    public interface IUserService
    {
        Task<UserBaseViewModel> GetUserProfileInformation(int id);
        Task<bool> UpdateUserDetailsFull(int userId, UserDetailsFullDTO data, bool saveChanges = true);
        Task<bool> UpdateUserDetails(int userId, UserDetailsDTO data, bool saveChanges = true);
        Task ConfirmRegistrationEmail(string token);
        Task<bool> InitPasswordRecovery(string email);
        Task ResetPassword(string token, string newPasswordHash);
        Task UploadProfilePicture(HttpPostedFileBase picture, int userId);
        Task SetDefaultProfilePicture(string defaultPictureName, int userId);
        Task<List<string>> ValidateUserAsync(User user);
        Task<List<string>> CreateAsync(User user);
        Task<bool> UpdateUserInfo(int userId, UserInfoDTO data, bool saveChanges = true);
        Task UpdateUserAndDetails(int userId, UserAndDetailsDTO data);
        Task<UserSettingsDTO> GetUserSettings(int userId);
    }
}
