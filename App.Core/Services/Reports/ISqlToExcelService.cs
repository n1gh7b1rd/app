﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Services
{
    public interface ISqlToExcelService
    {
        void RunSqlToExcel(string fileName, string queryOrProcedureName, string adoConnectionString, bool isProcedure);
    }
}
