﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Services
{
    public interface IReportService
    {
        Task<bool> RunReport(string reportName, string query, bool isProcedureQuery);
        /// <summary>
        /// 
        /// </summary>
        /// <param name="reportName">Current date is automatically appended to reportName when report is outputted.</param>
        /// <param name="query"></param>
        /// <param name="isProcedureQuery"></param>
        /// <returns></returns>
        Task<bool> ScheduleDailyRecurringReport(string reportName, string query, bool isProcedureQuery);
    }
}
