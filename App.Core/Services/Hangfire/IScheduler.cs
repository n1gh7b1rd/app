﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Services
{
    public interface IScheduler
    {
        string Enqueue(Expression<Action> methodCall);
        void EnqueueRecurringJob(string uniqueKey, Expression<Action> methodCall, string scheduleCron);
    }
}
