﻿using System.IO;

namespace App.Core.Services.Files
{
    public interface IFileStorageProvider
    {
        void WriteFile(string fileName, Stream content);
        Stream ReadFile(string fileName);
        string GetFileUrl(string fileName, bool isPublic = false);
        string GetReportFilePath(string fileName);
    }
}