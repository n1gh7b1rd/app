﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Services.Files
{
    public interface IFileStorageProviderFactory
    {
        IFileStorageProvider GetProvider();
    }
}
