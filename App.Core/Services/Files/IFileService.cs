﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Services.Files
{
    public interface IFileService
    {
        string GetFileUrl(string fileName);
        List<string> GetFileUrls(IEnumerable<string> fileNames);
        string GetRandomDefaultPictureName();
        Dictionary<string, string> GenerateDefaultProfilePicturesNamesUrlsMap();
        bool ValidatePictureFileType(string dotExtension);
        bool ValidateFileLength(int length);
        bool ValidateDefaultPictureName(string name);
        string GetTermsAndConditionsUrl();
        string GetPrivacyPolicyUrl();
    }
}
