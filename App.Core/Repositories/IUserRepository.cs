﻿using App.Entities;
using System;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace App.Core.Repositories
{
    public interface IUserRepository : IBaseRepository<User>
    {
        Task<User> GetByNameAsync(string userName, bool includeRoles = false);
        Task<bool> UpdateAsync(User user);
        Task<bool> CreateUserDetailsAsync(UserDetail userDetail);
        Task<TResult> GetByTokenAsync<TResult>(string token, Expression<Func<User, TResult>> selector);
    }
}
