﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Repositories
{
    public interface IClientAppRepository : IBaseRepository<ClientApp>
    {
        Task<ClientApp> FindClient(string name);
    }
}
