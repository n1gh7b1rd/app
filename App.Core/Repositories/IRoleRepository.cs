﻿using App.Entities;
using System.Threading.Tasks;

namespace App.Core.Repositories
{
    public interface IRoleRepository : IBaseRepository<Role>
    {
    }
}
