﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Repositories
{
    public interface IRefreshTokenRepository : IBaseRepository<RefreshToken>
    {
        Task<RefreshToken> FindRefreshTokenAsync(string token);
        Task<bool> CreateRefreshTokenAsync(RefreshToken token);
        Task<bool> RemoveRefreshToken(List<RefreshToken> oldTokens);
    }
}
