﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace App.Core.Repositories
{
    public interface IUserRoleRepository : IBaseRepository<UserRole>
    {
        Task<List<TResult>> GetUserRolesAsync<TResult>(int userId, Expression<Func<UserRole, TResult>> selector);
    }
}
