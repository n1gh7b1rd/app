﻿using App.Entities;
using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace App.Core.Repositories
{
    public interface IBaseRepository<TEntity> where TEntity : IEntity<int>
    {

        Task<bool> SaveChangesAsync();

        Task<TResult> GetByIdAsync<TResult>(int id, Expression<Func<TEntity, TResult>> selector);
        Task<TResult> GetByIdAsync<TResult, TNavProperty>(
            int id,
            Expression<Func<TEntity, TResult>> selector,
            Expression<Func<TEntity, TNavProperty>> selectorForInclude);

        Task<List<TEntity>> GetAllAsync();
        Task<List<TResult>> GetAllAsync<TResult>(Expression<Func<TEntity, TResult>> selector);
        Task<List<TResult>> GetAllAsync<TResult, TNavProperty>(
            Expression<Func<TEntity, TResult>> selector,
            Expression<Func<TEntity, TNavProperty>> selectorForInclude);

        void CreateWithoutSave(TEntity entity);
        void CreateWithoutSave(IEnumerable<TEntity> entities);

        Task<bool> CreateAsync(TEntity entity);
        Task<bool> CreateAllAsync(IEnumerable<TEntity> entities);

        void DeleteWithoutSave(TEntity entity);
        Task<bool> DeleteAsync(TEntity entity);
    }
}
