﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Core.Logging
{
    public interface ILogger
    {
        void WriteInfo(string message, string userName);
        void WriteError(string message, Exception exception, string userName = null);
        void WriteWarning(string message, string userName);
    }
}