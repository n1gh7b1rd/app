﻿using log4net;
using App.Core.Logging;
using System;

namespace App.Logging
{
    public class Log4NetLogger : ILogger
    {
        private ILog GetLogWriter()
        {
            var logger = LogManager.GetLogger("log4net");
            return logger;
        }

        public void WriteInfo(string message, string userName)
        {
            GetLogWriter().Info(message);
        }

        public void WriteError(string message, Exception exception, string userName = null)
        {
            GetLogWriter().Error(message, exception);
        }

        public void WriteWarning(string message, string userName)
        {
            GetLogWriter().Warn(message);
        }
    }
}
