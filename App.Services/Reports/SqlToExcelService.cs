﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using OfficeOpenXml;
using OfficeOpenXml.Style;
using System.IO;
using App.Core.Services.Files;
using App.Core.Services;

namespace App.Services.Reports
{
    public class SqlToExcelService: ISqlToExcelService
    {
        private readonly IFileStorageProviderFactory _fileStorageProviderFactory;
        public SqlToExcelService(IFileStorageProviderFactory fileStorageProviderFactory)
        {
            _fileStorageProviderFactory = fileStorageProviderFactory;
        }

        public void RunSqlToExcel(string fileName, string queryOrProcedureName, string adoConnectionString, bool isProcedure)
        {
            SqlConnection conn = new SqlConnection(adoConnectionString);
            conn.Open();
            SqlCommand cmd = new SqlCommand(queryOrProcedureName, conn);
            cmd.CommandType = isProcedure ? System.Data.CommandType.StoredProcedure : System.Data.CommandType.Text;
            SqlDataReader dr = cmd.ExecuteReader();

            string fullPathToOutputFile = _fileStorageProviderFactory.GetProvider().GetReportFilePath(fileName);
            var file = new FileInfo(fullPathToOutputFile + ".xlsx");

            using (ExcelPackage p = new ExcelPackage(file))
            {
                var worksheet = p.Workbook.Worksheets.Add(fileName);

                PopulateHeaderColumns(dr, worksheet);
                PopulateRows(dr, worksheet);

                p.Workbook.Properties.Title = fileName;
                p.Save();
            }
        }

        private static void PopulateRows(SqlDataReader dr, ExcelWorksheet worksheet)
        {
            int currentRow = 2; //1 is header
            while (dr.Read())
            {
                for (int i = 0; i < dr.FieldCount; i++)
                {
                    string value = dr[i].ToString();
                    worksheet.Cells[currentRow, i + 1].Value = value;
                }
                currentRow++;
            }
        }

        private static void PopulateHeaderColumns(SqlDataReader dr, ExcelWorksheet worksheet)
        {
            for (int i = 0; i < dr.FieldCount; i++)
            {
                string name = dr.GetName(i);
                worksheet.Cells[1, i + 1].Value = name;
            }
        }
    }
}
