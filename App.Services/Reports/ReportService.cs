﻿using Hangfire;
using App.Core.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Configuration;

namespace App.Services
{
    public class ReportService : IReportService
    {
        private readonly ISqlToExcelService _sqlToExcelService;
        private readonly IScheduler _scheduler;

        public ReportService(
            ISqlToExcelService sqlToExcelService,
             IScheduler scheduler)
        {
            _sqlToExcelService = sqlToExcelService;
            _scheduler = scheduler;
        }

        public async Task<bool> RunReport(string reportName, string query, bool isProcedureQuery)
        {
            //_sqlToExcelService.RunSqlToExcel(reportName, "ReportProcedure1", "AppAdoDbConnection", true);
            _sqlToExcelService.RunSqlToExcel(reportName, query, ConfigurationManager.ConnectionStrings["AppAdoDbConnection"].ConnectionString, isProcedureQuery);
            return await Task.FromResult(true);
        }

        public async Task<bool> ScheduleDailyRecurringReport(string reportName, string query, bool isProcedureQuery)
        {
            var scheduleKey = reportName;
            var scheduleCron = Cron.Daily();
            _scheduler.EnqueueRecurringJob(scheduleKey, () => _sqlToExcelService.RunSqlToExcel($"{reportName}-{DateTime.UtcNow.ToString("MM-dd-yyyy")}", query, ConfigurationManager.ConnectionStrings["AppAdoDbConnection"].ConnectionString, isProcedureQuery), scheduleCron);
            
            return await Task.FromResult(true);
        }

    }
}
