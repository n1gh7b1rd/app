﻿using App.Core.Logging;
using App.Core.Repositories;
using App.Core.Services;
using App.Entities;
using App.Models.User;
using System.Collections.Generic;
using System.Threading.Tasks;
using System;
using System.Net.Mail;
using App.Common.Enums;
using System.Web;
using App.Core.Services.Files;
using System.IO;
using App.Localization;
using System.Configuration;
using System.Linq;
using App.Common.Enums.Role;

namespace App.Services
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;
        private readonly ILogger _logger;
        private readonly IScheduler _scheduler;
        private readonly IFileStorageProviderFactory _fileStorageProviderFactory;
        private readonly IFileService _fileService;

        public UserService(
            IUserRepository userRepository,
            ILogger logger,
            IScheduler scheduler,
            IFileStorageProviderFactory fileStorageProviderFactory,
            IFileService fileService)
        {
            _userRepository = userRepository;
            _logger = logger;
            _scheduler = scheduler;
            _fileStorageProviderFactory = fileStorageProviderFactory;
            _fileService = fileService;
        }

        public async Task<UserBaseViewModel> GetUserProfileInformation(int id)
        {
            var result = await _userRepository.GetByIdAsync(id, UserBaseViewModel.Selector);
            //generate signed url to static file server/<path to picture>
            result.ProfilePicturePath = _fileService.GetFileUrl(result.ProfilePicturePath);
            return result;
        }

        public async Task<UserSettingsDTO> GetUserSettings(int userId)
        {
            UserSettingsDTO result = await _userRepository.GetByIdAsync(userId, UserSettingsDTO.UserToUserSettingsSelector);
            result.Picture = _fileService.GetFileUrl(result.Picture);

            return result;
        }

        public async Task<bool> UpdateUserInfo(int userId, UserInfoDTO data, bool saveChanges = true)
        {
            User user = await _userRepository.GetByIdAsync(userId, x => x);

            //Update only the values that are set
            user.FirstName = !string.IsNullOrWhiteSpace(data.FirstName) ? data.FirstName : user.FirstName;
            user.LastName = !string.IsNullOrWhiteSpace(data.LastName) ? data.LastName : user.LastName;
            user.PhoneNumber = !string.IsNullOrWhiteSpace(data.PhoneNumber) ? data.PhoneNumber : user.PhoneNumber;
            user.Updated = DateTime.UtcNow;
            if (saveChanges)
            {
                return await _userRepository.SaveChangesAsync();
            }

            return await Task.FromResult(true);
        }

        public async Task UpdateUserAndDetails(int userId, UserAndDetailsDTO data)
        {
            await this.UpdateUserInfo(userId, data.userInfo, false);
            await this.UpdateUserDetails(userId, data.userDetail, false);
            await _userRepository.SaveChangesAsync();
        }

        public async Task<bool> UpdateUserDetailsFull(int userId, UserDetailsFullDTO data, bool saveChanges = true)
        {
            UserDetail userDetails = await _userRepository.GetByIdAsync(userId, x => x.UserDetail);
            bool hasUserDetails = userDetails != null;

            if (!hasUserDetails)
            {
                userDetails = new UserDetail();
                userDetails.Id = userId;
                userDetails.Created = DateTime.UtcNow;
            }

            //Update only the values that are set
            userDetails.DateOfBirth = data.DateOfBirth.HasValue ? data.DateOfBirth.Value : userDetails.DateOfBirth;

            userDetails.PicturePath = !string.IsNullOrWhiteSpace(data.PicturePath) ? data.PicturePath : userDetails.PicturePath;

            userDetails.Updated = DateTime.UtcNow;

            if (!hasUserDetails)
            {
                return await _userRepository.CreateUserDetailsAsync(userDetails);
            }

            return await _userRepository.SaveChangesAsync();
        }

        public async Task<bool> UpdateUserDetails(int userId, UserDetailsDTO data, bool saveChanges = true)
        {
            UserDetailsFullDTO ud = new UserDetailsFullDTO();
            ud.GenderId = data.GenderId;
            ud.DateOfBirth = data.DateOfBirth;

            return await this.UpdateUserDetailsFull(userId, ud, saveChanges);
        }


        public async Task ConfirmRegistrationEmail(string token)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException("token", "Token cannot be null or empty.");
            }

            User user = await _userRepository.GetByTokenAsync(token, x => x);
            if (user == null)
            {
                throw new ArgumentException("Invalid token.");
            }

            user.ConfirmationToken = string.Empty;
            user.IsEmailConfirmed = true;

            await _userRepository.SaveChangesAsync();
        }

        public async Task<bool> InitPasswordRecovery(string email)
        {
            if (string.IsNullOrWhiteSpace(email))
            {
                throw new ArgumentNullException("email", "email cannot be null or empty.");
            }

            try
            {
                var m = new MailAddress(email);
            }
            catch (FormatException e)
            {
                throw new FormatException("Invalid email format.", e);
            }

            var user = await _userRepository.GetByNameAsync(email, true);

            if (user != null)
            {

                var recoveryGuid = Guid.NewGuid().ToString();
                user.ConfirmationToken = recoveryGuid;
                await _userRepository.SaveChangesAsync();
            }
            return await Task.FromResult(true);
        }

        public async Task ResetPassword(string token, string newPasswordHash)
        {
            if (string.IsNullOrEmpty(token))
            {
                throw new ArgumentNullException("token");
            }

            var user = await _userRepository.GetByTokenAsync(token, x => x);
            if (user == null)
            {
                throw new ArgumentException("Invalid token.");
            }

            if (string.IsNullOrEmpty(newPasswordHash))
            {
                throw new ArgumentNullException("newPasswordHash");
            }

            user.PasswordHash = newPasswordHash;
            user.ConfirmationToken = null;
            await _userRepository.SaveChangesAsync();
        }

        public async Task UploadProfilePicture(HttpPostedFileBase picture, int userId)
        {

            var dotExtension = Path.GetExtension(picture.FileName);
            if (!_fileService.ValidatePictureFileType(dotExtension))
            {
                throw new ArgumentException(Resources.InvalidFileExtension, "FileName");
            }

            if (!_fileService.ValidateFileLength(picture.ContentLength))
            {
                throw new ArgumentException(Resources.FileIsTooLarge, "ContentLength");
            }


            var newName = Guid.NewGuid().ToString() + dotExtension;

            UserDetailsFullDTO ud = new UserDetailsFullDTO();
            ud.PicturePath = newName;

            _fileStorageProviderFactory.GetProvider().WriteFile(newName, picture.InputStream);

            await this.UpdateUserDetailsFull(userId, ud);
        }

        public async Task SetDefaultProfilePicture(string defaultPictureName, int userId)
        {
            if (!_fileService.ValidateDefaultPictureName(defaultPictureName))
            {
                throw new ArgumentException(Resources.DefaultPictureDoesntExist);
            }

            UserDetailsFullDTO ud = new UserDetailsFullDTO();
            ud.PicturePath = defaultPictureName;

            await this.UpdateUserDetailsFull(userId, ud);
        }

        public async Task<List<string>> ValidateUserAsync(User user)
        {
            List<string> errorList = new List<string>();
            if (!IsValidEmail(user.UserName))
            {
                errorList.Add(Resources.InvalidEmailFormat);
            }

            if (await IsEmailInUse(user.UserName))
            {
                errorList.Add(Resources.EmailAlreadyExists);
            }

            return await Task.FromResult(errorList);
        }

        private async Task<bool> IsEmailInUse(string userName)
        {
            User user = await _userRepository.GetByNameAsync(userName);
            if (user != null)
            {
                return true;
            }

            return false;
        }

        private bool IsValidEmail(string email)
        {
            try
            {
                var m = new MailAddress(email);
            }
            catch (FormatException)
            {
                return false;
            }

            return true;
        }

        public async Task<List<string>> CreateAsync(User user)
        {
            List<string> errorList = new List<string>();
            errorList.AddRange(await ValidateUserAsync(user));
            if (errorList.Count > 0)
            {
                return errorList;
            }

            bool result = await _userRepository.CreateAsync(user);
            if (!result)
            {
                errorList.Add(Resources.ErrorInsertingToDatabase);
            }

            return errorList;
        }
    }
}
