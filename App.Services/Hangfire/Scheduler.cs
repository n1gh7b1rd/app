﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using App.Core.Services;
using System.Linq.Expressions;
using Hangfire;

namespace App.Services
{
    public class Scheduler : IScheduler
    {
        public string Enqueue(Expression<Action> methodCall)
        {
            return BackgroundJob.Enqueue(methodCall);
        }

        /// <summary> 
        ///var scheduleKey = $"automated_report_{reportName}_{accountId}_{userId}_{portfolioId}_{(int)day}";
        ///var scheduleCron = $"{DateTime.UtcNow.Minute} {DateTime.UtcNow.Hour} * * {(int)day}";
        /// </summary>
        /// <param name="uniqueKey"></param>
        /// <param name="methodCall"></param>
        /// <param name="scheduleCron"></param>
        public void EnqueueRecurringJob(string uniqueKey, Expression<Action> methodCall, string scheduleCron)
        {
            RecurringJob.AddOrUpdate(uniqueKey, methodCall, scheduleCron);
        }

    }
}
