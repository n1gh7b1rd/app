﻿using App.Core.Services;
using App.Core.Services.Files;
using System;
using System.Collections.Generic;
using System.Linq;

namespace App.Services.Files
{
    public class FileService : IFileService
    {
        private readonly IFileStorageProviderFactory _fileStorageProviderFactory;
        private readonly IConfigurationService _configuration;
        public FileService(IFileStorageProviderFactory fileStorageProviderFactory,
            IConfigurationService configuration)
        {
            _fileStorageProviderFactory = fileStorageProviderFactory;
            _configuration = configuration;
        }
        public string GetFileUrl(string fileName)
        {
            return _fileStorageProviderFactory.GetProvider().GetFileUrl(fileName);
        }

        public List<string> GetFileUrls(IEnumerable<string> fileNames)
        {
            var result = new List<string>();

            foreach (var file in fileNames)
            {
                result.Add(this.GetFileUrl(file));
            }

            return result;
        }

        private List<string> GetDefaultProfilePictureNames()
        {
            List<string> result = new List<string>();
            int length = int.Parse(_configuration.GetSetting("NumberOfDefaultProfilePictures"));
            for (int i = 1; i <= length; i++)
            {
                result.Add(GenerateDefaultPictureName(i));
            }
            return result;
        }

        public Dictionary<string, string> GenerateDefaultProfilePicturesNamesUrlsMap()
        {
            List<string> defaultPictureNames = this.GetDefaultProfilePictureNames();
            List<string> picUrls = this.GetFileUrls(defaultPictureNames);
            var dictionaryResult = defaultPictureNames.Zip(picUrls, (k, v) => new { Key = k, Value = v }).ToDictionary(x => x.Key, x => x.Value);

            return dictionaryResult;
        }
        public string GetRandomDefaultPictureName()
        {
            Random r = new Random();
            int length = int.Parse(_configuration.GetSetting("NumberOfDefaultProfilePictures"));
            int rInt = r.Next(1, length+1);
            string name = GenerateDefaultPictureName(rInt);
            return name;
        }

        private string GenerateDefaultPictureName(int number)
        {
            string name = $"default-{number}.png";
            return name;
        }

        public bool ValidatePictureFileType(string dotExtension)
        {
            string[] allowedExtensions = _configuration.GetSetting("AllowedFileExtensions").Split(',');
            if (!allowedExtensions.ToList().Contains(dotExtension))
            {
                return false;
            }

            return true;
        }

        public bool ValidateFileLength(int fileLength)
        {
            int maxFileLength = int.Parse(_configuration.GetSetting("UploadFileMaxLength"));
            if (fileLength > maxFileLength)
            {
                return false;
            }

            return true;
        }

        public bool ValidateDefaultPictureName(string name)
        {
            int numberOfExistingPics = int.Parse(_configuration.GetSetting("NumberOfDefaultProfilePictures"));

            try
            {
                var split = name.Split(new string[] { "default-" }, StringSplitOptions.None)[1].Split('.');
                var number = int.Parse(split[0]);
                var ext = split[1];

                if(number <1 || number > numberOfExistingPics)
                {
                    return false;
                }

                if(ext != "png")
                {
                    return false;
                }

                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public string GetTermsAndConditionsUrl()
        {
            var result = _fileStorageProviderFactory.GetProvider().GetFileUrl("Terms-and-Conditions.pdf", true);
            return result;
        }

        public string GetPrivacyPolicyUrl()
        {
            var result = _fileStorageProviderFactory.GetProvider().GetFileUrl("Privacy-Policy.pdf", true);
            return result;
        }
    }
}
