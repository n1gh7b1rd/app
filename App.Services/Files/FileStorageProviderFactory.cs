﻿using App.Core.Services;
using App.Core.Services.Files;
using System;
using System.Configuration;

namespace App.Services.Files
{
    public class FileStorageProviderFactory : IFileStorageProviderFactory
    {
        private readonly IConfigurationService _configuration;

        public FileStorageProviderFactory(IConfigurationService configuration)
        {
            _configuration = configuration;
        }

        public IFileStorageProvider GetProvider()
        {
            var providerName = _configuration.GetSetting("FileStorageProvider");
            switch (providerName)
            {   //TODO:
                //In the future when we move to Production hosting (AWS/Azure etc) there should be case where this provides
                //case "AWS/Azure": { return ProductionFileStorageProvider(); break;}
                default:
                    //this is used for non-dev environments, this is the folder where API will store files (on the StaticFileServer)
                    string baseFilePath = _configuration.GetSetting("SystemFilesFolderPath");  
                    string filesUrlName = _configuration.GetSetting("FilesUrlName"); 

#if DEBUG           //this expects App and App.StaticFileServer project folders to be in the same directory
                    //Example: "C:\\Dev\\Repositories\\AppWeb\\App\\App\\"
                    //it's done so the path is generated dynamically for each dev environment on each developer's pc
                    baseFilePath = $"{AppDomain.CurrentDomain.BaseDirectory.Replace($"App\\", "")}App.StaticFileServer\\{filesUrlName}\\";
#endif
                    return new DefaultFileStorageProvider(baseFilePath);
            }
        }
    }
}
