﻿using App.Core.Services.Files;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace App.Services.Files
{
    public class DefaultFileStorageProvider : IFileStorageProvider
    {
        private string _basePath;
        private string _secret;
        private string _staticServerUrl;
        private string _filesUrlName;
        private int _fileLinkExpirationSeconds;

        public DefaultFileStorageProvider(string basePath)
        {
            _basePath = basePath;
            _filesUrlName = ConfigurationManager.AppSettings["FilesUrlName"];
            _staticServerUrl = ConfigurationManager.AppSettings["StaticFileServer"];
            _secret = ConfigurationManager.AppSettings["FileLinkSecret"];
            _fileLinkExpirationSeconds = int.Parse(ConfigurationManager.AppSettings["FileLinkExpirationSeconds"]);
        }

        public void WriteFile(string fileName, Stream content)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            if (content == null)
            {
                throw new ArgumentNullException("content");
            }

            string fullpath = GetFilePath(fileName);
            using (FileStream fs = File.Create(fullpath))
            {
                content.CopyTo(fs);
            }
        }

        public Stream ReadFile(string fileName)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }

            string fullPath = GetFilePath(fileName);

            if (!File.Exists(fullPath))
            {
                throw new FileNotFoundException(fileName);
            }

            Stream result = new FileStream(fullPath, FileMode.Open, FileAccess.Read, FileShare.Read);
            return result;
        }

        public string GetFileUrl(string fileName, bool isPublic = false)
        {
            if (string.IsNullOrEmpty(fileName))
            {
                throw new ArgumentNullException("fileName");
            }
            string resource;
            string url;

            if (isPublic)
            {
                resource = $"/{_filesUrlName}/public/{fileName}";

                url = $"{_staticServerUrl}{resource}";

                return url;
            }
            
            resource = $"/{_filesUrlName}/{fileName}";

            url = $"{_staticServerUrl}{resource}";

            string secureUrl = string.Empty;

            long timestamp = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
            timestamp += _fileLinkExpirationSeconds;

            string base64token = this.ComputeToken(resource, timestamp);
            secureUrl = $"{url}?t={base64token}&e={timestamp}";

            return secureUrl;
        }

        private string GetFilePath(string fileName)
        {
            return Path.Combine(this._basePath, fileName);
        }
        public string GetReportFilePath(string fileName)
        {
            return Path.Combine($"{this._basePath}reports", fileName);
        }
        private string ComputeToken(string resource, long timestamp)
        {
            // build secret token used for checking request integrity on the static file server
            string secretString = string.Format("{0}{1} {2}", timestamp, resource, _secret);
            HashAlgorithm md5 = HashAlgorithm.Create("MD5");
            byte[] md5bytes = md5.ComputeHash(Encoding.ASCII.GetBytes(secretString));

            // create url-safe base64, see RFC4648
            string base64token = Convert.ToBase64String(md5bytes);
            base64token = base64token.Replace('+', '-').Replace('/', '_').TrimEnd('=');

            return base64token;
        }
    }
}
