﻿using App.Core.Services;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace App.Services
{
    public class ConfigurationService : IConfigurationService
    {
        public string GetSetting(string key)
        {
            var value = ConfigurationManager.AppSettings[key];
            return value;
        }
    }
}
