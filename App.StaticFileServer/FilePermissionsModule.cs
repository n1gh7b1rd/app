﻿using App.Localization;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;

namespace App.StaticFileServer
{
    public class FilePermissionsModule : IHttpModule
    {
        private readonly Regex PUBLIC_URL = new Regex(@"^/files/public", RegexOptions.Compiled | RegexOptions.IgnoreCase);

        public void Dispose()
        {

        }

        public void Init(HttpApplication context)
        {
            context.BeginRequest += context_BeginRequest;

        }

        void context_BeginRequest(object sender, EventArgs e)
        {
            HttpApplication app = sender as HttpApplication;
            try
            {
                // public files are not checked and are cached (ex. thumbnail)
                if (PUBLIC_URL.IsMatch(app.Request.Url.AbsolutePath))
                {
                    return;
                }

                if (app.Request.QueryString["t"] != null && app.Request.QueryString["e"] != null)
                {
                    // token is generated in FileStorageFS, used to verify request integrity
                    string token = app.Request.QueryString["t"];
                    token = this.GetToken(token);

                    // expiration is generated in FileStorageFS too
                    long expiration = long.Parse(app.Request.QueryString["e"]);
                    string resource = app.Request.Url.AbsolutePath;
                    string secret = ConfigurationManager.AppSettings["FileLinkSecret"];

                    this.ValidateToken(token, expiration, resource, secret);
                }
                else
                {
                    throw new ApplicationException(Resources.FileNotFound);
                }
            }
            catch (Exception ex)
            {
                app.Response.StatusCode = (int)HttpStatusCode.BadRequest;
                app.Response.StatusDescription = ex.Message;
                app.Response.End();
            }
            finally
            {
                // Allow CORS
                app.Response.Headers.Add("Access-Control-Allow-Origin", "*");
                app.Response.Headers.Add("Access-Control-Allow-Methods", "GET, HEAD, OPTIONS");
                app.Response.Headers.Add("Access-Control-Allow-Headers", "Range, X-Requested-With");
                app.Response.Headers.Add("Access-Control-Expose-Headers", "Accept-Ranges, Content-Encoding, Content-Length, Content-Range");
            }
        }

        private string GetToken(string token)
        {
            // token is encoded in url-safe base64, see RFC4648
            return token.Replace('-', '+').Replace('_', '/').PadRight(token.Length + 2, '=');
        }

        private void ValidateToken(string token, long expiration, string resource, string secret)
        {
            // build secret string to compare with token
            string secretString = $"{expiration}{resource} {secret}";
            HashAlgorithm md5 = HashAlgorithm.Create("MD5");
            byte[] md5bytes = md5.ComputeHash(Encoding.ASCII.GetBytes(secretString));
            string base64token = Convert.ToBase64String(md5bytes);

            //byte[] md5bytes = MD5.Create().ComputeHash(Encoding.ASCII.GetBytes(secretString));
            //string md5base64 = Convert.ToBase64String(md5bytes);
            long timestamp = (long)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

            if (token != base64token)
            {
                throw new ApplicationException("Invalid token.");
            }

            if (timestamp > expiration)
            {
                throw new ApplicationException("Token expired.");
            }
        }
    }
}