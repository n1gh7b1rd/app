﻿using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.OAuth;
using App.Core.Repositories;
using App.Entities;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using App.Common.Enums;
using System.Collections.Generic;
using System.Linq;
using App.Common.Enums.Role;
using System;
using App.Common.Enums.Extensions;
using App.Localization;
using App.Logging;

namespace App.Security.OAuth
{
    public class AppOAuthProvider : OAuthAuthorizationServerProvider
    {
        private readonly Func<IUnityContainer> _getUnityContainerChildFunc;
        private Log4NetLogger _logger = new Log4NetLogger();
        public AppOAuthProvider(Func<IUnityContainer> getUnityContainerChildFunc)
            : base()
        {
            _getUnityContainerChildFunc = getUnityContainerChildFunc;
        }

        public override async Task ValidateClientAuthentication(OAuthValidateClientAuthenticationContext context)
        {
            try
            {
                string clientId = string.Empty;
                string clientSecret = string.Empty;
                ClientApp client = null;

                if (!context.TryGetBasicCredentials(out clientId, out clientSecret))
                {
                    context.TryGetFormCredentials(out clientId, out clientSecret);
                }

                if (context.ClientId == null)
                {
                    //Remove the comments from the below line context.SetError, and invalidate context 
                    //if you want to force sending clientId/secrects once obtain access tokens. 
                    context.Validated();
                    //context.SetError("invalid_clientId", "ClientId should be sent.");
                    return; //Task.FromResult<object>(null);
                }

                using (var container = _getUnityContainerChildFunc.Invoke())
                {
                    var _clientAppRepo = container.Resolve<IClientAppRepository>();
                    client = await _clientAppRepo.FindClient(context.ClientId);
                }

                if (client == null)
                {
                    context.SetError("invalid_clientId", string.Format("Client '{0}' is not registered in the system.", context.ClientId));
                    return; //Task.FromResult<object>(null);
                }

                if (client.ClientApplicationTypeId == (int)ClientApplicationTypesEnum.Mobile)
                {
                    if (string.IsNullOrWhiteSpace(clientSecret))
                    {
                        context.SetError("invalid_clientId", "Client secret should be sent.");
                        return; //Task.FromResult<object>(null);
                    }
                    else
                    {
                        if (client.Secret != clientSecret)
                        {
                            context.SetError("invalid_clientId", "Client secret is invalid.");
                            return; //Task.FromResult<object>(null);
                        }
                    }
                }

                if (!client.Active)
                {
                    context.SetError("invalid_clientId", "Client is inactive.");
                    return; //Task.FromResult<object>(null);
                }

                context.OwinContext.Set<string>("as:clientAllowedOrigin", client.AllowedOrigin);
                context.OwinContext.Set<string>("as:clientRefreshTokenLifeTime", client.RefreshTokenLifeTime.ToString());

                context.Validated();
                return; //Task.FromResult<object>(null);
            }
            catch (Exception e)
            {
                _logger.WriteError(e.Message, e, "oauth provider validate");
                throw e;
            }
        }

        public override async Task GrantResourceOwnerCredentials(OAuthGrantResourceOwnerCredentialsContext context)
        {
            try
            {
                var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");

                if (allowedOrigin == null) allowedOrigin = "*";

                //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

                var userManager = context.OwinContext.GetUserManager<AppUserManager>();

                User user = await userManager.FindAsync(context.UserName, context.Password);

                if (user == null)
                {
                    context.SetError("invalid_grant", Resources.InvalidUserNameOrPassword);
                    return;
                }

                if (!user.IsEmailConfirmed)
                {
                    context.SetError("invalid_grant", Resources.EmailNotConfirmed);
                    return;
                }

                var identity = await GenerateUserIdentityAsync(user, userManager, context.Options.AuthenticationType);

                var props = new AuthenticationProperties(new Dictionary<string, string>
                {
                    {
                        "as:client_id", (context.ClientId == null) ? string.Empty : context.ClientId
                    },
                    {
                        "userName", context.UserName
                    }
                });

                var ticket = new AuthenticationTicket(identity, props);
                context.Validated(ticket);
            }
            catch (Exception e)
            {

                _logger.WriteError(e.Message, e, "oauth provider grant");
                throw e;
            }
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(User user, AppUserManager manager, string authenticationType)
        {
            var userIdentity = await manager.CreateIdentityAsync(user, authenticationType);
            var userRoles = user.UserRoles;
            var roleIds = userRoles.Select(ur => ur.RoleId).ToList();
            var filteredRoles = EnumExtensions.GetFilteredEnumsValues<RoleEnum>(roleIds);

            foreach (var val in filteredRoles)
            {
                userIdentity.AddClaim(new Claim(ClaimTypes.Role, val.ToString()));
            }

            return userIdentity;
        }

        public override Task GrantRefreshToken(OAuthGrantRefreshTokenContext context)
        {
            try
            {
                var originalClient = context.Ticket.Properties.Dictionary["as:client_id"];
                var currentClient = context.ClientId;

                if (originalClient != currentClient)
                {
                    context.SetError("invalid_clientId", "Refresh token is issued to a different clientId.");
                    return Task.FromResult<object>(null);
                }

                // Change auth ticket for refresh token requests
                var newIdentity = new ClaimsIdentity(context.Ticket.Identity);
                //newIdentity.AddClaim(new Claim("newClaim", "newValue"));

                var newTicket = new AuthenticationTicket(newIdentity, context.Ticket.Properties);
                context.Validated(newTicket);

                return Task.FromResult<object>(null);
            }
            catch (Exception e)
            {
                _logger.WriteError(e.Message, e, "oauth provider grant refresh");
                throw e;
            }
        }
    }
}