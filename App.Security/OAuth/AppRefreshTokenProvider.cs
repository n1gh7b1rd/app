﻿using Microsoft.Owin.Security.Infrastructure;
using App.Common.Utility;
using App.Core.Repositories;
using App.Entities;
using System;
using System.Threading.Tasks;
using Microsoft.Practices.Unity;
using App.Logging;

namespace App.Security.OAuth
{
    public class AppRefreshTokenProvider : IAuthenticationTokenProvider
    {
        private readonly Func<IUnityContainer> _getUnityContainerChildFunc;
        private Log4NetLogger _logger = new Log4NetLogger();

        public AppRefreshTokenProvider(Func<IUnityContainer> getUnityContainerChildFunc)
            : base()
        {
            _getUnityContainerChildFunc = getUnityContainerChildFunc;
        }

        public async Task CreateAsync(AuthenticationTokenCreateContext context)
        {
            try
            {
                var clientid = context.Ticket.Properties.Dictionary["as:client_id"];

                if (string.IsNullOrEmpty(clientid))
                {
                    return;
                }

                var refreshTokenGuid = Guid.NewGuid().ToString("n");

                using (var container = _getUnityContainerChildFunc.Invoke())
                {
                    var refreshTokenLifeTime = context.OwinContext.Get<string>("as:clientRefreshTokenLifeTime");
                    var _refreshTokenRepo = container.Resolve<IRefreshTokenRepository>();
                    var _clientAppRepo = container.Resolve<IClientAppRepository>();
                    var client = await _clientAppRepo.FindClient(clientid);
                    var token = new RefreshToken()
                    {
                        Token = HashUtility.GetSHA256Hash(refreshTokenGuid),
                        ClientAppId = client.Id,
                        UserName = context.Ticket.Identity.Name,
                        IssuedUtc = DateTime.UtcNow,
                        ExpiresUtc = DateTime.UtcNow.AddMinutes(Convert.ToDouble(refreshTokenLifeTime))
                    };

                    context.Ticket.Properties.IssuedUtc = token.IssuedUtc;
                    context.Ticket.Properties.ExpiresUtc = token.ExpiresUtc;

                    token.ProtectedTicket = context.SerializeTicket();

                    var result = await _refreshTokenRepo.CreateRefreshTokenAsync(token);

                    if (result)
                    {
                        context.SetToken(refreshTokenGuid);
                    }
                }
            }
            catch (Exception e)
            {
                _logger.WriteError(e.Message, e, "refresh token provider create");
                throw e;
            }
        }

        public async Task ReceiveAsync(AuthenticationTokenReceiveContext context)
        {
            try
            {
                var allowedOrigin = context.OwinContext.Get<string>("as:clientAllowedOrigin");
                //context.OwinContext.Response.Headers.Add("Access-Control-Allow-Origin", new[] { allowedOrigin });

                string hashedTokenValue = HashUtility.GetSHA256Hash(context.Token);

                using (var container = _getUnityContainerChildFunc.Invoke())
                {
                    var _refreshTokenRepo = container.Resolve<IRefreshTokenRepository>();

                    var refreshToken = await _refreshTokenRepo.FindRefreshTokenAsync(hashedTokenValue);

                    if (refreshToken != null)
                    {
                        //Get protectedTicket from refreshToken class
                        context.DeserializeTicket(refreshToken.ProtectedTicket);
                        var result = await _refreshTokenRepo.RemoveRefreshToken(refreshToken.InList());
                    }
                }
            }
            catch (Exception e)
            {

                _logger.WriteError(e.Message, e, "refresh token provider receive");
                throw e;
            }
        }

        public void Receive(AuthenticationTokenReceiveContext context)
        {
            //!!! NOT SURE THIS IS FINE
            this.ReceiveAsync(context).Wait();
        }

        public void Create(AuthenticationTokenCreateContext context)
        {
            //!!! NOT SURE THIS IS FINE
            this.CreateAsync(context).Wait();
        }

    }
}