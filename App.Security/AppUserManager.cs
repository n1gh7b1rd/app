﻿using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using App.Core.Services;
using App.Entities;
using System.Collections.Generic;

namespace App.Security
{
    public class AppUserManager : UserManager<User, int>
    {
        private readonly IUserService _userService;

        public AppUserManager(
            IUserStore<User, int> store,
            IUserService userService)
            : base(store)
        {
            _userService = userService;
            this.UserValidator = new AppUserValidator(this, userService);
        }

        public async override Task<IdentityResult> CreateAsync(User user)
        {
            List<string> errors = await _userService.CreateAsync(user);
            if (errors.Count > 0)
            {
                return await Task.FromResult(IdentityResult.Failed(errors.ToArray()));
            }

            return await Task.FromResult(IdentityResult.Success);
        }
    }

}
