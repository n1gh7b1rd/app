﻿using Microsoft.AspNet.Identity;
using App.Core.Services;
using App.Entities;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace App.Security
{
    public class AppUserValidator : UserValidator<User, int>
    {
        public AppUserManager _userManager { get; set; }
        private readonly IUserService _userService;

        public AppUserValidator(
            UserManager<User, int> manager,
            IUserService userService)
            : base(manager)
        {
            _userManager = (AppUserManager)manager;
            _userService = userService;
            this.AllowOnlyAlphanumericUserNames = false;
            this.RequireUniqueEmail = true;
        }

        public async override Task<IdentityResult> ValidateAsync(User user)
        {
            if (user.Id == 0) //If we are creating a new user then validate.
            {
                List<string> validationResult = await _userService.ValidateUserAsync(user);
                if (validationResult.Count > 0)
                {
                    return await Task.FromResult(IdentityResult.Failed(validationResult.ToArray()));
                }
            }

            return await Task.FromResult(IdentityResult.Success);
        }


    }
}
