﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using App.Entities;
using App.Core.Repositories;
using System.Collections.Generic;
using App.Common.Enums.Role;
using App.Common.Enums.Extensions;

namespace App.Security
{
    public class AppUserStore :
        IUserStore<User, int>,
        IUserPasswordStore<User, int>,
        IUserRoleStore<User, int>
    {
        private readonly IUserRepository _userRepository;
        private readonly IUserRoleRepository _userRoleRepository;

        public AppUserStore(
            IUserRepository userRepository,
            IUserRoleRepository userRoleRepository)
        {
            _userRepository = userRepository;
            _userRoleRepository = userRoleRepository;
        }

        public async Task CreateAsync(User user)
        {
            if (user != null)
            {
                await _userRepository.CreateAsync(user);
            }
            else
            {
                throw new ArgumentNullException("user");
            }
        }

        public async Task DeleteAsync(User user)
        {
            if (user != null)
            {
                await _userRepository.DeleteAsync(user);
            }
            else
            {
                throw new ArgumentNullException("user");
            }
        }

        public void Dispose()
        {
        }

        public Task<string> GetPasswordHashAsync(User user)
        {
            return Task.FromResult(user.PasswordHash);
        }

        public Task<bool> HasPasswordAsync(User user)
        {
            return Task.FromResult(user.PasswordHash != null);
        }

        public async Task<User> FindByIdAsync(int userId)
        {
            return await _userRepository.GetByIdAsync(userId, x => x);
        }

        public async Task<User> FindByNameAsync(string userName)
        {
            return await _userRepository.GetByNameAsync(userName, true);
        }

        public Task SetPasswordHashAsync(User user, string passwordHash)
        {
            user.PasswordHash = passwordHash;
            return Task.FromResult(0);
        }

        public async Task UpdateAsync(User user)
        {
            if (user != null)
            {
                await _userRepository.UpdateAsync(user);
            }
            else
            {
                throw new ArgumentNullException("user");
            }
        }

        public async Task AddToRoleAsync(User user, string roleName)
        {
            RoleEnum role = roleName.ToEnum<RoleEnum>();
            var userRole = new UserRole
            {
                UserId = user.Id,
                RoleId = (int)role
            };
            await _userRoleRepository.CreateAsync(userRole);
        }

        public async Task RemoveFromRoleAsync(User user, string roleName)
        {
            RoleEnum role = roleName.ToEnum<RoleEnum>();
            var userRole = new UserRole
            {
                UserId = user.Id,
                RoleId = (int)role
            };
            await _userRoleRepository.DeleteAsync(userRole);
        }

        public async Task<IList<string>> GetRolesAsync(User user)
        {
            var roles = await _userRoleRepository.GetUserRolesAsync(user.Id, x => x.Role.Name);
            return roles;
        }

        public async Task<bool> IsInRoleAsync(User user, string roleName)
        {
            var roles = await _userRoleRepository.GetUserRolesAsync(user.Id, x => x.Role.Name);
            bool result = roles.Contains(roleName);
            return result;
        }
    }
}
